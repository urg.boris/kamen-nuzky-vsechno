-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 15, 2022 at 10:53 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `knv`
--

-- --------------------------------------------------------

--
-- Table structure for table `fights`
--

DROP TABLE IF EXISTS `fights`;
CREATE TABLE IF NOT EXISTS `fights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  `beginVotes` int(11) NOT NULL,
  `endVotes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hand1` (`id1`),
  KEY `hand2` (`id2`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `fights`
--

INSERT INTO `fights` (`id`, `id1`, `id2`, `beginVotes`, `endVotes`) VALUES
(1, 1, 2, 1, 0),
(2, 2, 3, 1, 0),
(3, 2, 1, 1, 0),
(4, 32, 34, 1, 0),
(5, 2, 32, 1, 0),
(6, 1, 32, 1, 1),
(7, 3, 1, 1, 0),
(8, 31, 32, 1, 0),
(9, 39, 41, 1, 0),
(10, 41, 35, 1, 0),
(11, 37, 42, 2, 1),
(12, 2, 42, 1, 1),
(13, 42, 41, 1, 0),
(14, 36, 42, 1, 0),
(15, 42, 3, 1, 0),
(16, 32, 41, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hands`
--

DROP TABLE IF EXISTS `hands`;
CREATE TABLE IF NOT EXISTS `hands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `search_name` varchar(255) COLLATE utf8_czech_ci NOT NULL,
  `sound` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Dumping data for table `hands`
--

INSERT INTO `hands` (`id`, `name`, `search_name`, `sound`, `image`) VALUES
(1, 'kamen', 'kamen', NULL, NULL),
(2, 'nuzky', 'nuzky', NULL, NULL),
(3, 'papir', 'papir', NULL, NULL),
(31, 'Plechovka', 'Plechovka', 'sound/31.mp3', NULL),
(32, 'čekuláda', 'cekulada', 'sound/32.mp3', NULL),
(33, 'ščřšěžřčžřčěžčřšČĚŘČŠ', 'scrsezrczrcezcrscercs', 'sound/33.mp3', NULL),
(34, 'ščřšěžřčřšČĚŘČŠ', 'scrsezrcrscercs', 'sound/34.mp3', 'sound/34.mp3'),
(35, 'povlinka', 'povlinka', NULL, NULL),
(36, 'fsdfsd', 'fsdfsd', NULL, NULL),
(37, '43545', '43545', NULL, NULL),
(38, '43545fds', '43545fds', NULL, NULL),
(39, 'fsdfasd', 'fsdfasd', 'sound/39.mp3', 'images/39.png'),
(40, 'fsdaf', 'fsdaf', NULL, 'images/40.png'),
(41, 'porod', 'porod', 'sound/41.mp3', 'images/41.png'),
(42, 'humš', 'hums', 'sound/42.mp3', 'images/42.png');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fights`
--
ALTER TABLE `fights`
  ADD CONSTRAINT `hand1` FOREIGN KEY (`id1`) REFERENCES `hands` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `hand2` FOREIGN KEY (`id2`) REFERENCES `hands` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
