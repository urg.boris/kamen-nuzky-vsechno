<?php

declare(strict_types=1);

namespace App\Presenters;

require_once(__DIR__ . '/../model/voicerss_tts.php');
require_once(__DIR__ . '/../model/random_image.php');
require_once(__DIR__ . '/../model/GoogleImageGrabber.php');

use Nette;
use Nette\Application\UI\Form;
use Nette\Http\FileUpload;
use Buchin\GoogleImageGrabber\GoogleImageGrabber;
use RandomImageGenerator\RandomImageGenerator;

final class ApiPresenter extends Nette\Application\UI\Presenter {

    private \App\Model\MysqlHandler $dbHandler;

    /** @var \Nette\Database\Context @inject */
    public $db;

    function __construct(\App\Model\MysqlHandler $neo4jHandler) {

        $this->dbHandler = $neo4jHandler;
    }

    //aby se pres axios neposila request 2x
    protected function startup(): void {
        parent::startup();
        if ($this->getHttpRequest()->getMethod() == 'OPTIONS') {
            $this->ResponseJsonWithHeaders([]);
        }
    }

    function actionDefault() {
        /*   $filePath = "images/687";
          $createdImage = $this->GetGoogleImage("palačinka", $filePath);
          echo '<img src="/' . $createdImage . '">';
          die; */
    }

    function CreateImageForNewHand($keyword, $filePathWithoutExtension) {
        $result = $this->GetGoogleImage($keyword, $filePathWithoutExtension);
        if($result!==false) {
            return $result;
        }
        return $this->GenerateRandomImage($filePathWithoutExtension);
    }

    function GenerateRandomImage($filePathWithoutExtension) {
        try {
            $finalImagePath = $filePathWithoutExtension . ".png";
            $imageGen = new RandomImageGenerator(100, 100, 5);
            $imageGen->GenerateAndSave($finalImagePath);
            return $finalImagePath;
        } catch (\Exception $e) {
            return false;
        }
    }

    function GetGoogleImage($keyword, $filePathWithoutExtension) {
        try {
            $finalImagePath = $filePathWithoutExtension . ".png";
            \Tracy\Debugger::log("grab new  ");
            $images = GoogleImageGrabber::grab($keyword);
            if(count($images)==0) {
                return false;
            }
            $index = mt_rand(0, count($images) - 1);
            $url = $images[$index];
            \Tracy\Debugger::log("ulr  " . $url);
            $imageData = file_get_contents($url);
            \Tracy\Debugger::log("writing new  ");
            \Nette\Utils\FileSystem::write($filePathWithoutExtension . ".jpg", $imageData);
            $image = \Nette\Utils\Image::fromFile($filePathWithoutExtension . ".jpg");
            \Tracy\Debugger::log("save new  ");
            $image->save($finalImagePath, null, \Nette\Utils\Image::PNG);
            \Nette\Utils\FileSystem::delete($filePathWithoutExtension . ".jpg");
            return $finalImagePath;
        } catch (\Exception $e) {
            return false;
        }
    }

    function actionCreateBaseDB() {
        $this->dbHandler->CreateBaseBD();
    }

    function actionGetAllConnections() {
        $connections = $this->dbHandler->GetAllConnections();
        $this->ResponseJsonWithHeaders($connections);
    }

    function actionGetConnection($id1, $id2) {
        $connection = $this->dbHandler->GetConnection($id1, $id2);
        $this->ResponseJsonWithHeaders($connection);
    }

    function createComponentNewHandForm() {
        $form = new Form;

        $form->setAction($this->link(":addHand"));
        $form->addText('name', 'Jméno');
        
        $form->addUpload('image', 'obrazek')
               ->addRule(Form::MIME_TYPE, "obrazek", ['image/png'] );
            $form->addUpload('sound', 'sound');
              //      ->addRule(Form::MIME_TYPE, "zvuk", ['audio/mp3, application/octet-stream//'] );
        //$form->onSuccess[] = [$this, 'success'];
$form->addSubmit('submit', 'Uložit');
        return $form;
    }
/*
    public function success(Form $form, $values) {
        echo "success 1";
    }
*/
    /*
      public function formSucceeded(Form $form, Nette\Utils\ArrayHash $values) {
      $entityId = $this->getParameter('id');

      if ($entityId) {
      $post = $this->db->table(self::ENTITY_TABLE_NAME)->get($entityId);
      $post->update($values);
      } else {
      $post = $this->db->table(self::ENTITY_TABLE_NAME)->insert($values);
      $entityId = $post->id;
      }
      $this->flashMessage('Uloženo. ' . date("Y-m-d H:i:s"), 'success');
      $this->redirect(':Edit', $entityId);
      }

     */

    function ResponseJsonWithHeaders($array) {

        $this->getHttpResponse()->setHeader('Access-Control-Allow-Origin', '*');
        $this->getHttpResponse()->setHeader('Access-Control-Allow-Headers', '*');
        $this->getHttpResponse()->setHeader('Expires', '0');
        $this->getHttpResponse()->setHeader('Cache-Control', "must-revalidate, post-check=0, pre-check=0");

        $this->sendResponse(new \Nette\Application\Responses\JsonResponse($array));
    }

    function actionNewHandForm() {
        
    }

    function actionAddNewHandTest($name) {
        $this->redirect(":addHand");
    }

    function CreateSound($text, $soundPath) {

        $tts = new \VoiceRSS;
        $voiceData = $tts->speech([
            'key' => 'a1d3f710109948018733b20077470515',
            'hl' => 'cs-cz',
            'v' => 'Linda',
            'src' => \App\Model\AHandler::RemoveInvalidCharacters("   " . $text), //'Poblitá bota!',
            'r' => '0',
            'c' => 'mp3',
            'f' => '44khz_16bit_stereo',
            'ssml' => 'false',
            'b64' => 'false'
        ]);
        if($voiceData["response"]==false) {
            return false;
        }
        \Nette\Utils\FileSystem::write($soundPath, $voiceData["response"]);
        return true;
        // file_put_contents($soundPath, $voiceData);
    }

    function actionAddHand() {
        $rawBody = $this->getHttpRequest()->getRawBody();
        $post = $this->getHttpRequest()->getPost();
        $image = $this->getHttpRequest()->getFile("image");
        $sound = $this->getHttpRequest()->getFile("sound");


        //\Tracy\Debugger::log($rawBody);
        // $image =  $files->  // array_key_exists("image", $files) ? $files["image"] : null;
        //   $sound = array_key_exists("sound", $files) ? $files["sound"] : null;

        $name = $post["name"];
        \Tracy\Debugger::log("--------adding new name" . $name);
        \Tracy\Debugger::log("image + sound file");
        \Tracy\Debugger::log($image);
        \Tracy\Debugger::log($sound);
        $newhand = $this->dbHandler->AddNewHand($name);
        $newHandId = $newhand->id;
        $imagePath = null;

        $imageFilePathWithoutExtension = "images/" . $newHandId;
        $imageFilePath = $imageFilePathWithoutExtension . ".png";
        $createdImage = false;
        if ($image) {
            if ($image->hasFile() && $image->isOk()) {
                try {
                    \Nette\Utils\FileSystem::delete($imageFilePath);
                    $image->move($imageFilePath);
                    $createdImage = $imageFilePath;
                } catch (\Exception $e) {
                    \Tracy\Debugger::log("image failed");
                    $imageCreated = false;// $this->CreateImageForNewHand($name, $imageFilePathWithoutExtension);
                }
            }
        } else {
            \Tracy\Debugger::log("creating new image " . $imageFilePathWithoutExtension);
            $createdImage = $this->CreateImageForNewHand($name, $imageFilePathWithoutExtension);
        }

        $soundFilePath = "sound/" . $newHandId . ".mp3";
        $soundCreated = false;
        \Tracy\Debugger::log("sound handle " . $soundFilePath);
        if ($sound) {
            if ($sound->hasFile() && $sound->isOk()) {
                try {
                    \Nette\Utils\FileSystem::delete($soundFilePath);
                    $sound->move($soundFilePath);
                    $soundCreated = true;
                } catch (\Exception $e) {
                    $soundCreated = false;
                }
            }
        } else {
            
        }

        if (!$soundCreated) {
            \Tracy\Debugger::log("create sound " . $soundFilePath);
            try {                
                $soundCreated = $this->CreateSound(Nette\Utils\Strings::normalize($name), $soundFilePath);
            } catch (\Exception $e) {
                \Tracy\Debugger::log("failed " . $e->getMessage());
                $soundCreated = false;
            }
        }

        $dbResult = $this->db->table(\App\Model\MysqlHandler::TABLE_HANDS)->get($newHandId)->update(
                [
                    "image" => $createdImage ? $imageFilePath : null,
                    "sound" => $soundCreated ? $soundFilePath : null,
                ]
        );



        // \Tracy\Debugger::log($post["againstId"] ." - " . $post["name"] );
        //  \Tracy\Debugger::log("starting addind connection");
        //$hands = $this->neo4jHandler->AddNewHandWithConnection($name, $againstId);
        //  \Tracy\Debugger::log("done addind connection");
        $this->ResponseJsonWithHeaders($newhand->ToArray());


        //$this->neo4jHandler->AddHand($name, $againstId);
        //$this->neo4jHandler->AddHandRandomly();
    }

    function actionList($reload = false) {
        $handsList = $this->dbHandler->GetList($reload);
        $this->ResponseJsonWithHeaders($handsList);
    }

    function actionFight($aiHand, $userHand) {
        $winner = $this->dbHandler->EvaluateFight($aiHand, $userHand);

        $this->ResponseJsonWithHeaders($winner);
    }

    function actionIncrementVote($forNodeId, $connectedToNode) {
        $connection = $this->dbHandler->IncrementVote($forNodeId, $connectedToNode);
        $this->ResponseJsonWithHeaders($connection);
    }

    function actionGetRandomHand($excludeId = -1) {
        $handId = $this->dbHandler->GetAIRandomHandId($excludeId);
        $this->ResponseJsonWithHeaders($handId);
    }

    function actionAddRandomHand() {
        //$this->neo4jHandler->AddHand($name, $againstId);
        $hand = $this->dbHandler->AddHandRandomly();
        $this->ResponseJsonWithHeaders($hand->ToArray());
    }

    function actionAddRandomFight() {
        //$this->neo4jHandler->AddHand($name, $againstId);
        $hands = $this->dbHandler->AddRandomFight();
        $this->ResponseJsonWithHeaders($hands);
    }

}
