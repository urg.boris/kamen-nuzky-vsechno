<?php

namespace App\Model;

interface IHandler {
    
    public function CreateBaseBD();
    public function GetList($forceDBReload = false);
    public function IncrementVote($forNodeId, $connectedToNode);
    public function GetAllConnections();
    public function GetConnection($id1, $id2);
    public function EvaluateFight($AIHandId, $userHandId);
    public function AddHandRandomly();
    public function GetAIRandomHandId($excludeId = -1);
    public function AddRandomFight();
    public function AddHand($name);
    public function AddNewHandWithConnection($name, $againstId);

    
}

