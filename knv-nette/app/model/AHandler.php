<?php

declare(strict_types=1);

namespace App\Model;

abstract class AHandler {

    function make_seed() {
        list($usec, $sec) = explode(' ', microtime());
        return $sec + $usec * 1000000;
    }

    function readable_random_string($length = 6) {
        $conso = array("b", "c", "d", "f", "g", "h", "j", "k", "l",
            "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z");

        $vocal = array("a", "e", "i", "o", "u");
        $password = "";
       //srand();
        $max = $length / 2;

        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        return $password;
    }

    function GetRandomIndexFromArray($arr) {
        $randIndex = mt_rand(0, count($arr) - 1);
        return $randIndex;
    }

    function GetRandomElementFromArray($arr) {
        $randIndex = $this->GetRandomIndexFromArray($arr);
        return $arr[$randIndex];
    }

    function GetWinnerStateJson($winnerState, $winnerId) {
        return [
            "winnerState" => $winnerState,
            "winnerId" => $winnerId,
        ];
    }
    
    public static function RemoveInvalidCharacters($input, $allowedChars = 'abcdefghijklmnopqrstuvwxyzěščřžýáíéúůňďťĚŠČŘŽÝÁÍÉÚŮĎŤŇ 1234567890')
    {
        $str = '';
        $input=trim($input);
        for ($i   = 0; $i < strlen($input); $i++)
        {
            if (!stristr($allowedChars, $input[$i]))
            {
                continue;
            }

            $str .= $input[$i];
        }

        return $str;
    }

    function CreateSearchName($name) {
        return $this->RemoveInvalidCharacters($name);
    }

}
