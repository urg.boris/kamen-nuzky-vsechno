<?php

namespace App\Model;

use Nette;
use Laudis\Neo4j\Authentication\Authenticate;
use Laudis\Neo4j\ClientBuilder;
use Nette\Caching\Cache;

//TODO fspritnf
class Hand {

    public $id;
    public $name;
    public $image;
    public $searchName;

    public function __construct($dataArray) {
        $this->id = $dataArray["id"];
        $this->name = $dataArray["name"];
        $this->sound = array_key_exists("sound", $dataArray) ? $dataArray["sound"] : "";
        $this->image = array_key_exists("image", $dataArray) ? $dataArray["image"] : "";
        $this->searchName = array_key_exists("search_name", $dataArray) ? $dataArray["search_name"] : "";
    }

    public function ToArray() {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "searchName" => $this->searchName
        ];
    }

    static function CreateEmpty($id) {
        return new Hand(["id" => $id, "name" => "noname"]);
    }

}

class HandsFight {

    public $id;
    public Hand $beginHand;
    public $beginVotes;
    public Hand $endHand;
    public $endVotes;

    public function __construct($beginHand, $endHand, $dataArray) {
        $this->beginHand = $beginHand;
        $this->endHand = $endHand;
        $this->id = array_key_exists("id", $dataArray) ? intval($dataArray["id"]) : -1;
        $this->beginVotes = array_key_exists("beginVotes", $dataArray) ? intval($dataArray["beginVotes"]) : 0;
        $this->endVotes = array_key_exists("endVotes", $dataArray) ? intval($dataArray["endVotes"]) : 0;
    }

    public function NoVotes() {
        return $this->beginVotes == 0 && $this->endVotes == 0;
    }

    static function CreateEmpty($idA, $idB, $dataArray = []) {
        return new HandsFight(Hand::CreateEmpty($idA), Hand::CreateEmpty($idB), $dataArray);
    }

    public function ToArray() {
        return [
            "beginNode" => $this->beginHand->toArray(),
            "endNode" => $this->endHand->ToArray(),
            "beginVotes" => $this->beginVotes,
            "endVotes" => $this->endVotes,
        ];
    }

}

final class MysqlHandler extends AHandler {

    use Nette\SmartObject;

    /** @var \Nette\Database\Context */
    public $db;

    public const VOTES_BEGIN = "beginVotes";
    public const VOTES_END = "endVotes";
    public const TABLE_HANDS = "hands";
    public const TABLE_FIGHTS = "fights";

    private $cache;

    const HAND_LIST_CACHE = "HandCache";
    const HAND_LIST_KEY = "HandList";
    const WINNER_STATE_AI = "ai";
    const WINNER_STATE_USER = "user";
    const WINNER_STATE_NOONE = "noone";

    public function __construct(\Nette\Database\Context $db) {
        $this->db = $db;
    }

    /*
      public function IsDBConnected() {
      return $this->neo4jDB->verifyConnectivity();
      }

      public function CheckDBConnection() {
      if (!$this->IsDBConnected()) {
      throw new \Exception("db not found");
      }
      }
     */

    public function CreateBaseBD() {

        if (\Tracy\Debugger::$productionMode === false) {
            $this->db->query("SET FOREIGN_KEY_CHECKS=0;");
            $this->db->table(self::TABLE_HANDS)->delete();
            $this->db->table(self::TABLE_HANDS)->insert([
                [
                    "id" => 1,
                    "name" => "kámen",
                    "search_name" => "kamen",
                    "image"=>"images/1.png",
                    "sound" => "sound/1.mp3",
                ],
                [
                    "id" => 2,
                    "name" => "nůžky",
                    "search_name" => "nuzky",
                    "image"=>"images/2.png",
                    "sound" => "sound/2.mp3",
                ],
                [
                    "id" => 3,
                    "name" => "papír",
                    "search_name" => "papir",
                    "image"=>"images/3.png",
                    "sound" => "sound/3.mp3",
                ]
            ]);
            $this->db->table(self::TABLE_FIGHTS)->delete();
            $this->db->table(self::TABLE_FIGHTS)->insert([
                [
                    "id" => 1,
                    "id1" => 1,
                    "id2" => 2,
                    "beginVotes" => 1,
                    "endVotes" => 0,
                ],
                [
                    "id" => 2,
                    "id1" => 2,
                    "id2" => 3,
                    "beginVotes" => 1,
                    "endVotes" => 0,
                ],
                [
                    "id" => 3,
                    "id1" => 2,
                    "id2" => 1,
                    "beginVotes" => 1,
                    "endVotes" => 0,
                ]
            ]);
            $this->db->query("SET FOREIGN_KEY_CHECKS=0;");
            die("ok");
        }
    }

    public function GetList($forceDBReload = true) {
        $handsList = [];
        $results = $this->db->table(self::TABLE_HANDS)->select("id, name, search_name");

        foreach ($results as $result) {
            $hand = new Hand($result->toArray());
            $handsList[] = $hand;
        }

        return $handsList;
    }

    public function GetAllConnections() {
        $connections = [];
        $results = $this->db->table(self::TABLE_FIGHTS); //->select("id, id1, id2, beginVotes, endVotes");

        foreach ($results as $result) {
            $hand1Data = $result->ref('hands', 'id1')->toArray();
            $hand2Data = $result->ref('hands', 'id2')->toArray();
            /* \Tracy\Debugger::dump($hand2Data);
              die; */
            $hand1 = new Hand($hand1Data);
            $hand2 = new Hand($hand2Data);

            $fight = new HandsFight($hand1, $hand2, $result->toArray()); // Hand($result["id"], $result["name"]);
            $connections[] = $fight->toArray();
        }

        return $connections;
    }

    function NodesDoExist($idA, $idB) {
        if ($idA == $idB) {
            throw new \Exception("a==b no fight");
        }
        $id1 = min([$idA, $idB]);
        $id2 = max([$idA, $idB]);

        $results = $this->db->table(self::TABLE_HANDS)->where("id=? OR id=?", $id1, $id2);
        if ($results->count() != 2) {
            return false;
        } else {
            return true;
        }
    }

    function GetVotesForTwoHands($AIHandId, $userHandId) {
        if ($AIHandId == $userHandId) {
            throw new \Exception("chyba-remiza");
        }

        /* $id1 = min([$AIHandId, $userHandId]);
          $id2 = max([$userHandId, $AIHandId]); */
        $results = $this->db->table(self::TABLE_FIGHTS)->where("(id1=? AND id2=?) OR (id1=? AND id2=?) ", $AIHandId, $userHandId, $userHandId, $AIHandId)->limit(1);

        $fightExists = $results->count() != 0;


        $votesForhands;
        if (!$fightExists) {
            $votesForhands = HandsFight::CreateEmpty($AIHandId, $userHandId);
            /*      \Tracy\Debugger::dump($votesForhands);
              die; */
            return $votesForhands;
        }

        $result = $results->fetch();

        $hand1Data = $result->ref('hands', 'id1')->toArray();
        $hand2Data = $result->ref('hands', 'id2')->toArray();

        $nodeA = new Hand($hand1Data);
        $nodeB = new Hand($hand2Data);
        $votesForhands;
        $votesForhands = new HandsFight($nodeA, $nodeB,
                [
            "id" => $result["id"],
            self::VOTES_BEGIN => $result[self::VOTES_BEGIN],
            self::VOTES_END => $result[self::VOTES_END],
                ]
        );
        /* if ($userHandId == $result["id1"]) {
          $votesForhands = new HandsFight($nodeA, $nodeB,
          [
          "id" => $result["id"],
          self::VOTES_BEGIN => $result[self::VOTES_BEGIN],
          self::VOTES_END => $result[self::VOTES_END],
          ]
          );
          } else {
          $votesForhands = new HandsFight($nodeB, $nodeA,
          [
          "id" => $result["id"],
          self::VOTES_BEGIN => $result[self::VOTES_END],
          self::VOTES_END => $result[self::VOTES_BEGIN],
          ]
          );
          } */

        return $votesForhands;
    }

    function AddFightBetweenHands($id1, $id2) {
        if ($id1 == $id2) {
            throw new \Exception("chyba-cant add fight");
        }
        $results = $this->db->table(self::TABLE_FIGHTS)->where("(id1=? AND id2=?) OR (id1=? AND id2=?)", $id1, $id2, $id2, $id1);


        $fightExists = $results->count() > 0;

        if ($fightExists) {
            die("those two already have connection");
        }


        $newCon = $this->db->table(self::TABLE_FIGHTS)->insert([
            "id1" => $id1,
            "id2" => $id2,
            self::VOTES_BEGIN => 1,
            self::VOTES_END => 0,
        ]);

        $hands = [];
        $result = $newCon;

        $newFight = HandsFight::CreateEmpty($id1, $id2, $result->toArray());

        //new HandItem($node->id(), $node->getProperty('name'));

        return [$newFight->beginHand, $newFight->endHand];
    }

    public function EvaluateFight($AIHandId, $userHandId) {
        /* \Tracy\Debugger::dump($AIHandId);
          \Tracy\Debugger::dump($userHandId);
          die; */

        if ($AIHandId == $userHandId) {
            return $this->GetWinnerStateJson(self::WINNER_STATE_NOONE, $AIHandId);
        }
        if (!$this->NodesDoExist($AIHandId, $userHandId)) {
            throw new \Exception("nodes cant fight - dont exist " . $AIHandId . " " . $userHandId);
        }

        $handConnection = $this->GetVotesForTwoHands($AIHandId, $userHandId);
        /* \Tracy\Debugger::dump($handConnection);
          die; */

        if ($handConnection->NoVotes()) {
            if (mt_rand(0, 1) == 1) {
                $handNodes = $this->AddFightBetweenHands($AIHandId, $userHandId);

                $winnerId = $handNodes[0]->id;
                return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $winnerId);
            } else {
                $handNodes = $this->AddFightBetweenHands($userHandId, $AIHandId);

                $winnerId = $handNodes[0]->id;
                return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $winnerId);
            }
        } else {

            if ($handConnection->beginVotes == $handConnection->endVotes) {
                /*  \Tracy\Debugger::dump($handConnection->beginVotes);
                  die; */

                $userWins = mt_rand(0, 1) == 1;
                $winnerState;
                $updateData;
                $userHandId = intval($userHandId);
                $AIHandId = intval($AIHandId);



                if ($userWins) {
                    if ($userHandId == $handConnection->beginHand->id) {
                        $winnerId = $handConnection->beginHand->id;
                        $updateData = [
                            self::VOTES_BEGIN => $handConnection->beginVotes + 1
                        ];
                        $winnerState = $this->GetWinnerStateJson(self::WINNER_STATE_USER, $winnerId);
                    } else {
                        $winnerId = $handConnection->endHand->id;
                        $updateData = [
                            self::VOTES_END => $handConnection->endVotes + 1
                        ];
                        $winnerState = $this->GetWinnerStateJson(self::WINNER_STATE_USER, $winnerId);
                    }
                } else {
                    if ($AIHandId == $handConnection->beginHand->id) {
                        $winnerId = $handConnection->beginHand->id;
                        $updateData = [
                            self::VOTES_BEGIN => $handConnection->beginVotes + 1
                        ];
                        $winnerState = $this->GetWinnerStateJson(self::WINNER_STATE_AI, $winnerId);
                    } else {
                        $winnerId = $handConnection->endHand->id;
                        $updateData = [
                            self::VOTES_END => $handConnection->endVotes + 1
                        ];
                        $winnerState = $this->GetWinnerStateJson(self::WINNER_STATE_AI, $winnerId);
                    }
                }
                /*          \Tracy\Debugger::dump($userHandId);
                  \Tracy\Debugger::dump($AIHandId);
                  \Tracy\Debugger::dump($handConnection);
                  \Tracy\Debugger::dump($winnerState);
                  \Tracy\Debugger::dump($updateData);
                  die; */


                $updateRes = $this->db
                        ->table(self::TABLE_FIGHTS)
                        ->get($handConnection->id)
                        ->update($updateData);
                return $winnerState;
            }

            if ($handConnection->beginVotes > $handConnection->endVotes) {
                if ($handConnection->beginHand->id == $AIHandId) {
                    return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $AIHandId);
                } else {
                    return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $userHandId);
                }
            } else { // end is wiiner
                if ($handConnection->endHand->id == $AIHandId) {
                    return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $AIHandId);
                } else {
                    return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $userHandId);
                }
            }
        }
    }

//TODO everit, ze funguje
    public function IncrementVote($forNodeId, $connectedToNode) {

        if ($forNodeId == $connectedToNode) {
            throw new \Exception("chyba-same ids");
        }

        /* $id1 = min([$AIHandId, $userHandId]);
          $id2 = max([$userHandId, $AIHandId]); */
        $results = $this->db->table(self::TABLE_FIGHTS)->where("(id1=? AND id2=?) OR (id1=? AND id2=?) ", $forNodeId, $connectedToNode, $connectedToNode, $forNodeId)->limit(1);

        $fightExists = $results->count() != 0;


        if (!$fightExists) {
            throw new \Exception("no connection  between=" . $forNodeId . "-" . $connectedToNode);
        }

        $handConnection = $results->fetch();
        $fightData = $handConnection->toArray();
        /*
          \Tracy\Debugger::dump($fightData);
          die; */

        /*
          $hand1Data = $result->ref('hands', 'id1')->toArray();
          $hand2Data = $result->ref('hands', 'id2')->toArray();
         */
        $oldNodeAData = $handConnection->ref('hands', 'id1')->toArray();

        $oldNodeBData = $handConnection->ref('hands', 'id2')->toArray();
        /*                                    \Tracy\Debugger::dump($oldNodeAData["id"]);
          die; */
        $oldNodeA = new Hand($oldNodeAData);
        $oldNodeB = new Hand($oldNodeBData);
        $fight = new HandsFight($oldNodeA, $oldNodeB, $fightData);

        $oldNodeAId = $oldNodeA->id;
        $oldNodeBId = $oldNodeB->id;


        $oldNodeAVotes = $fight->beginVotes;
        $oldNodeBVotes = $fight->endVotes;

        $newNodeAVotes = $oldNodeAVotes;
        $newNodeBVotes = $oldNodeBVotes;
        $newNodeA = '';
        $newNodeB = '';

        $updatedConnection;

        $updateData = null;
        if ($forNodeId == $oldNodeAId) {
            $newNodeAVotes = $oldNodeAVotes + 1;
            $updateData = [
                self::VOTES_BEGIN => $newNodeAVotes,
            ];
        } else {
            $newNodeBVotes = $oldNodeBVotes + 1;
            $updateData = [
                self::VOTES_END => $newNodeBVotes,
            ];
        }
        /* if ($oldNodeAVotes < $oldNodeBVotes) {
          $newNodeA = 'b';
          $newNodeB = 'a';
          $newNodeAVotes = $oldNodeBVotes;
          $newNodeBVotes = $oldNodeAVotes;
          $updatedConnection = new HandsConnection(
          HandItem::CreateEmpty($oldNodeBId),
          HandItem::CreateEmpty($oldNodeAId),
          $newNodeAVotes,
          $newNodeBVotes
          );
          } else {
          $newNodeA = 'a';
          $newNodeB = 'b';
          $newNodeAVotes = $oldNodeAVotes;
          $newNodeBVotes = $oldNodeBVotes;
          $updatedConnection = new HandsConnection(
          HandItem::CreateEmpty($oldNodeAId),
          HandItem::CreateEmpty($oldNodeBId),
          $newNodeAVotes,
          $newNodeBVotes,
          );
          } */
        //add vote to begin node
        /* } else {
          $oldNodeBVotes += 1;
          if ($oldNodeAVotes < $oldNodeBVotes) {
          $newNodeA = 'b';
          $newNodeB = 'a';
          $newNodeAVotes = $oldNodeBVotes;
          $newNodeBVotes = $oldNodeAVotes;
          $updatedConnection = new HandsConnection(
          HandItem::CreateEmpty($oldNodeBId),
          HandItem::CreateEmpty($oldNodeAId),
          $newNodeAVotes,
          $newNodeBVotes,
          );
          } else {
          $newNodeA = 'a';
          $newNodeB = 'b';
          $newNodeAVotes = $oldNodeAVotes;
          $newNodeBVotes = $oldNodeBVotes;
          $updatedConnection = new HandsConnection(
          HandItem::CreateEmpty($oldNodeAId),
          HandItem::CreateEmpty($oldNodeBId),
          $newNodeAVotes,
          $newNodeBVotes,
          );
          }
          //add vote to end node
          } */

        $res = $this->db->table(self::TABLE_FIGHTS)->get($handConnection["id"])->update($updateData);
        $updatedConnection = new HandsConnection(
                HandItem::CreateEmpty($oldNodeBId),
                HandItem::CreateEmpty($oldNodeAId),
                $newNodeAVotes,
                $newNodeBVotes,
        );

        if ($res != 1) {
            throw new \Exception("votes did not update");
        }

        return $updatedConnection->ToArray();
    }

    public function GetConnection($id1, $id2, $asObject = false) {
        $results = $this->db->table(self::TABLE_FIGHTS)->where("(id1=? AND id2=?) OR (id1=? AND id2=?)", $id1, $id2, $id2, $id1);

        if (count($results) == 0) {
            return ["error" => "no connection"];
        }

        $result = $results->fetch();

        $hand1DB = $result->ref('hands', 'id1');
        $hand1Data;
        $hand2Data;
        $handConnectionData;
        if ($hand1DB["id"] == $id1) {
            $hand1Data = $result->ref('hands', 'id1')->toArray();
            $hand2Data = $result->ref('hands', 'id2')->toArray();
            $handConnectionData = [
                "id" => $result["id"],
                self::VOTES_BEGIN => $result[self::VOTES_BEGIN],
                self::VOTES_END => $result[self::VOTES_END],
            ];
        } else {
            $hand2Data = $result->ref('hands', 'id1')->toArray();
            $hand1Data = $result->ref('hands', 'id2')->toArray();
            $handConnectionData = [
                "id" => $result["id"],
                self::VOTES_BEGIN => $result[self::VOTES_END],
                self::VOTES_END => $result[self::VOTES_BEGIN],
            ];
        }



        $nodeA = new Hand($hand1Data);
        $nodeB = new Hand($hand2Data);

        $connection = new HandsFight(
                $nodeA,
                $nodeB,
                $handConnectionData
        );
        if ($asObject) {
            return $connection;
        } else {
            return $connection->ToArray();
        }
    }

    function GetWinnerStateJson($winnerState, $winnerId) {
        return [
            "winnerState" => $winnerState,
            "winnerId" => $winnerId,
        ];
    }

    public function AddHandRandomly() {
        $randomHand = $this->db->table(self::TABLE_HANDS)->order("RAND()")->limit(1)->fetch();



        $randHandId = $randomHand["id"];
        /*    \Tracy\Debugger::dump($randHandId);
          die;
         */
        $name = ucfirst($this->readable_random_string(mt_rand(4, 8)));
        $newHand = $this->AddNewHandWithConnection($name, $randHandId);
        return new Hand($newHand->toArray());
    }

    public function GetAIRandomHandId($excludeId = -1) {
        $list = $this->GetList();
        if ($excludeId != -1) {
            unset($list[$excludeId]);
        }
        $randKey = array_rand($list);
        return $list[$randKey];
    }

    public function AddRandomFight() {
        throw new Exception("not implemented");
        /*
          $results = $this->neo4jDB->run('
          MATCH (a:hand), (b:hand)
          WHERE NOT (a)-[:' . self::BASE_REL_TYPE . ']-(b:hand) AND a<>b
          WITH a, b
          ORDER BY rand()
          LIMIT 1
          CREATE (a)-[r:' . self::BASE_REL_TYPE . '{' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b)
          RETURN   a,b ');
          if (count($results) == 0) {
          die("cant add new fight, everything connected");
          }
          $hands = [];
          foreach ($results as $result) {
          // Returns a Node
          $node = $result->get('a');
          $hands[] = $this->CreateHandItemFormDBNode($node); // new HandItem($node->id(), $node->getProperty('name'));
          $node = $result->get('b');
          $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));
          }
          return $hands; */
    }

    function createSearchName($name) {
        setlocale(LC_ALL, "en_US.utf8");
        $name = trim(mb_strtolower($name));
        $name = iconv('utf-8', 'ASCII//TRANSLIT', $name);
        return $name;
    }

    public function AddHand($name) {
        
    }

    public function AddNewHand($name) {
        $result = $this->db->table(self::TABLE_HANDS)->where("search_name = ?", $this->createSearchName($name));


        if ($result->count() > 0) {
            throw new \Exception("this hand already exists");
        }

        $result = $this->db->table(self::TABLE_HANDS)->insert(
                [
                    "name" => $name,
                    "search_name" => $this->createSearchName($name),
                ]
        );

        $newHandData = $result->toArray();
        $newHand = new Hand($newHandData);

        //$hands[] = $newHand->ToArray(); //new HandItem($node->id(), $node->getProperty('name'));

        return $newHand;
    }

    public function AddNewHandWithConnection($name, $againstId) {
        $result = $this->db->table(self::TABLE_HANDS)->insert(
                [
                    "name" => $name,
                    "search_name" => $this->createSearchName($name),
                ]
        );

        $newHandData = $result->toArray();
        $newHand = new Hand($newHandData);
        $newFight;
        if (mt_rand(0, 1) == 1) {
            $newFight = [
                "id1" => $newHand->id,
                "id2" => $againstId,
                self::VOTES_BEGIN => 1,
                self::VOTES_END => 0
            ];
        } else {
            $newFight = [
                "id1" => $newHand->id,
                "id2" => $againstId,
                self::VOTES_BEGIN => 0,
                self::VOTES_END => 1
            ];
        }

        $this->db->table(self::TABLE_FIGHTS)->insert($newFight);
        /*
          $againstHand = new Hand($this->db->table(self::TABLE_HANDS)->get($againstId)->toArray());
          \Tracy\Debugger::dump($againstHand);
          \Tracy\Debugger::dump($newHand);
          die; */
        return $newHand;
    }

}
