<?php

namespace App\Model;

use Nette;
use Laudis\Neo4j\Authentication\Authenticate;
use Laudis\Neo4j\ClientBuilder;
use Nette\Caching\Cache;

class HandItem {

    public $id;
    public $name;
    public $img;

    public function __construct($id, $name) {
        $this->id = $id;
        $this->name = $name;
    }

    public function ToArray() {
        return [
            "id" => $this->id,
            "name" => $this->name
        ];
    }

    static function CreateEmpty($id) {
        return new HandItem($id, "noname");
    }

}

class HandsConnection {

    public $id;
    public HandItem $beginHand;
    public $beginVotes;
    public HandItem $endHand;
    public $endVotes;

    public function __construct($beginHand, $endHand, $beginVotes, $endVotes) {
        $this->beginHand = $beginHand;
        $this->endHand = $endHand;
        $this->beginVotes = $beginVotes;
        $this->endVotes = $endVotes;
    }

    public function NoVotes() {
        return $this->beginVotes == 0 && $this->endVotes == 0;
    }

    static function CreateEmpty($idA, $idB) {
        return new HandsConnection(HandItem::CreateEmpty($idA), HandItem::CreateEmpty($idB), 0, 0);
    }

    public function ToArray() {
        return [
            "beginNode" => $this->beginHand->toArray(),
            "endNode" => $this->endHand->ToArray(),
            "beginVotes" => $this->beginVotes,
            "endVotes" => $this->endVotes,
        ];
    }

}

final class Neo4jHandler extends AHandler implements IHandler {

    use Nette\SmartObject;

    private \Laudis\Neo4j\Client $neo4jDB;

    public const BASE_REL_TYPE = "IS_STRONGER_THAN";
    const VOTES_BEGIN = "beginVotes";
    const VOTES_END = "endVotes";

    private $cache;

    const HAND_LIST_CACHE = "HandCache";
    const HAND_LIST_KEY = "HandList";
    const WINNER_STATE_AI = "ai";
    const WINNER_STATE_USER = "user";
    const WINNER_STATE_NOONE = "noone";

    public function __construct($neo4jurl, Nette\Caching\IStorage $storage) {
        $this->cache = new Cache($storage, self::HAND_LIST_CACHE);
/*
        $conn = new \Bolt\connection\StreamSocket('85091a9e.databases.neo4j.io');
        $conn->setSslContextOptions([
            'verify_peer' => true,
    'allow_self_signed' => true
        ]);
         $this->neo4jDB = new \Bolt\Bolt($conn);
         
         $this->neo4jDB->setProtocolVersions(4.1);
         $this->neo4jDB->hello(\Bolt\helpers\Auth::basic('neo4j', 'zxVDKbL8NlfWTq2vnU0oIapmSLXaAQ6cc0UEmqQKOh'));
        //$this->neo4jDB->init('aura', 'neo4j', 'zxVDKbL8NlfWTq2vnU0oIapmSLXaAQ6cc0UEmqQKOh');
  */
        /*
        
        $this->neo4jDB = ClientBuilder::create()
                ->withDriver(
                        'aura',
                        'neo4j://85091a9e.databases.neo4j.io:7687',
                        Authenticate:: basic('neo4j', 'zxVDKbL8NlfWTq2vnU0oIapmSLXaAQ6cc0UEmqQKOh')
                ) // creates an http driver
                // ->withDriver('neo4j', 'neo4j://neo4j.test.com?database=my-database', Authenticate::kerberos('token')) // creates an auto routed driver
                ->withDefaultDriver('aura')
                ->build();*/
        
          $this->neo4jDB = ClientBuilder::create()
          ->withDriver('local', $neo4jurl) // creates a bolt driver
          //->withDriver('bolt', 'bolt+s://neo4j:root@localhost')
          //->withDriver('https', 'https://test.com', Authenticate::basic('user', 'password')) // creates an http driver
          // ->withDriver('neo4j', 'neo4j://neo4j.test.com?database=my-database', Authenticate::kerberos('token')) // creates an auto routed driver
          ->withDefaultDriver('local')
          ->build(); 
        /*
          $this->neo4jDB = ClientBuilder::create()
          ->withDriver('local', $neo4jurl)
          ->withDefaultDriver('local')
          ->build(); */
//$this->RemoveCachedList();
        // $this->CheckDBConnection();
    }

    public function IsDBConnected() {
        return $this->neo4jDB->verifyConnectivity();
    }

    public function CheckDBConnection() {
        if (!$this->IsDBConnected()) {
            throw new \Exception("db not found");
        }
    }

    public function CreateBaseBD() {
        if (\Tracy\Debugger::$productionMode === false) {
            $results = $this->neo4jDB->run('
                    MATCH (a)
                    DETACH DELETE a
            ');
            /* if (count($results) == 0) {
              die("no hands");
              } */
            $createNewDB = '
CREATE (a:hand{name: "Kamen"}), (b:hand{name: "Nuzky"}), (c:hand{name: "Papir"})
CREATE 
    (a)-[r1:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b),
    (b)-[r2:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(c),
    (c)-[r3:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(a)
RETURN a, b, c;
            ';

            /* CREATE (a:hand{name: "Kamen"}), (b:hand{name: "Nuzky"}), (c:hand{name: "Papir"})
              CREATE
              (a)-[r1:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b),
              (b)-[r2:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(c),
              (c)-[r3:' . self::BASE_REL_TYPE . ' {' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(a)
              RETURN a, b, c; */
            $results = $this->neo4jDB->run($createNewDB);
            $this->RemoveCachedList();
            die("ok");
        }
    }

    /*
      public function GetListForSelectBox() {
      $results = $this->neo4jDB->run('MATCH (n) RETURN n');

      $handsList = [];
      foreach ($results as $result) {
      // Returns a Node
      $node = $result->get('n');
      $handsList[$node->id()] = $node->getProperty('name');
      }
      return $handsList;
      } */

    public function RemoveCachedList() {
        $this->cache->save(self::HAND_LIST_KEY, null);
    }

    public function GetList($forceDBReload = false) {
        $cachedList = $this->cache->load(self::HAND_LIST_KEY);
        $handsList = [];
        if ($cachedList === NULL || $forceDBReload == true) {
            $results = $this->neo4jDB->run('MATCH (n) RETURN n');

            $handsList = [];
            foreach ($results as $result) {
                // Returns a Node
                $node = $result->get('n');
                //$hand = new HandItem($node->id(), $node->getProperty('name'));
                $handsList[$node->id()] = $this->CreateHandItemFormDBNode($node);
                //$handsList[] = $hand;


                $this->cache->save(self::HAND_LIST_KEY, $handsList);
            }
        } else {
            $handsList = $cachedList;
        }

        return $handsList;
    }

    function readable_random_string($length = 6) {
        $conso = array("b", "c", "d", "f", "g", "h", "j", "k", "l",
            "m", "n", "p", "r", "s", "t", "v", "w", "x", "y", "z");

        $vocal = array("a", "e", "i", "o", "u");
        $password = "";
        srand((double) microtime() * 1000000);
        $max = $length / 2;

        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        return $password;
    }

    function GetRandomIndexFromArray($arr) {
        $randIndex = mt_rand(0, count($arr) - 1);
        return $randIndex;
    }

    function GetRandomElementFromArray($arr) {
        $randIndex = $this->GetRandomIndexFromArray($arr);
        return $arr[$randIndex];
    }

    function GetVotesForTwoHands($AIHandId, $userHandId) {
        if ($AIHandId == $userHandId) {
            throw new \Exception("chyba-remiza");
        }
        $query = '
MATCH (a:hand)-[w:' . self::BASE_REL_TYPE . ']->(b:hand)
WHERE id(a)=' . $AIHandId . ' AND id(b)=' . $userHandId . ' OR id(a)=' . $userHandId . ' AND id(b)=' . $AIHandId . '
RETURN a, b, id(w) AS id, w.' . self::VOTES_BEGIN . ' AS ' . self::VOTES_BEGIN . ', w.' . self::VOTES_END . ' AS ' . self::VOTES_END . '
';
        /* \Tracy\Debugger::dump($query);
          die; */
        $results = $this->neo4jDB->run($query);


        $votesForhands;
        if (count($results) == 0) {
            $votesForhands = HandsConnection::CreateEmpty($AIHandId, $userHandId);
            /* new HandsConnection(
              HandItem::CreateEmpty($AIHandId),
              HandItem::CreateEmpty($userHandId),
              0,
              0); */
            /* \Tracy\Debugger::dump($votesForhands);
              die; */
            return $votesForhands;
        }

        $result = $results->first();
        $nodeA = $this->CreateHandItemFormDBNode($result->get('a'));
        $nodeB = $this->CreateHandItemFormDBNode($result->get('b'));


        $votesForhands = new HandsConnection(
                $nodeA,
                $nodeB,
                $result->get(self::VOTES_BEGIN),
                $result->get(self::VOTES_END),
        );


        return $votesForhands;
    }

//TODO everit, ze funguje
    public function IncrementVote($forNodeId, $connectedToNode) {
        $query = '
            MATCH (a:hand)-[w:' . self::BASE_REL_TYPE . ']->(b:hand)
            WHERE id(a)=' . $forNodeId . ' AND id(b)=' . $connectedToNode . ' OR id(a)=' . $connectedToNode . ' AND id(b)=' . $forNodeId . '
            return a,b,w.beginVotes as beginVotes, w.endVotes as endVotes';
        /* \Tracy\Debugger::dump($query);
          die; */
        $results = $results = $this->neo4jDB->run($query);
        if (count($results) == 0) {
            throw new \Exception("no connection  between=" . $forNodeId . "-" . $connectedToNode);
        }
        $result = $results->first();
        $oldNodeAId = $result->get("a")->id();
        $oldNodeBId = $result->get("b")->id();
        $oldNodeAVotes = intval($result->get("beginVotes"));
        $oldNodeBVotes = intval($result->get("endVotes"));

        $newNodeAVotes = 0;
        $newNodeBVotes = 0;
        $newNodeA = '';
        $newNodeB = '';

        $updatedConnection;

        if ($forNodeId == $oldNodeAId) {
            $oldNodeAVotes += 1;
            if ($oldNodeAVotes < $oldNodeBVotes) {
                $newNodeA = 'b';
                $newNodeB = 'a';
                $newNodeAVotes = $oldNodeBVotes;
                $newNodeBVotes = $oldNodeAVotes;
                $updatedConnection = new HandsConnection(
                        HandItem::CreateEmpty($oldNodeBId),
                        HandItem::CreateEmpty($oldNodeAId),
                        $newNodeAVotes,
                        $newNodeBVotes
                );
            } else {
                $newNodeA = 'a';
                $newNodeB = 'b';
                $newNodeAVotes = $oldNodeAVotes;
                $newNodeBVotes = $oldNodeBVotes;
                $updatedConnection = new HandsConnection(
                        HandItem::CreateEmpty($oldNodeAId),
                        HandItem::CreateEmpty($oldNodeBId),
                        $newNodeAVotes,
                        $newNodeBVotes,
                );
            }
            //add vote to begin node
        } else {
            $oldNodeBVotes += 1;
            if ($oldNodeAVotes < $oldNodeBVotes) {
                $newNodeA = 'b';
                $newNodeB = 'a';
                $newNodeAVotes = $oldNodeBVotes;
                $newNodeBVotes = $oldNodeAVotes;
                $updatedConnection = new HandsConnection(
                        HandItem::CreateEmpty($oldNodeBId),
                        HandItem::CreateEmpty($oldNodeAId),
                        $newNodeAVotes,
                        $newNodeBVotes,
                );
            } else {
                $newNodeA = 'a';
                $newNodeB = 'b';
                $newNodeAVotes = $oldNodeAVotes;
                $newNodeBVotes = $oldNodeBVotes;
                $updatedConnection = new HandsConnection(
                        HandItem::CreateEmpty($oldNodeAId),
                        HandItem::CreateEmpty($oldNodeBId),
                        $newNodeAVotes,
                        $newNodeBVotes,
                );
            }
            //add vote to end node
        }

        $query = '
            match (a)-[w]->(b)
            WHERE id(a)=' . $oldNodeAId . ' AND id(b)=' . $oldNodeBId . '
            delete w 
            create 
                (' . $newNodeA . ')-
                [r:' . self::BASE_REL_TYPE . ' {beginVotes:' . $newNodeAVotes . ', endVotes:' . $newNodeBVotes . '}]
                ->(' . $newNodeB . ') 
            return a,r, b';


        $results = $results = $this->neo4jDB->run($query);
        if (count($results) == 0) {
            throw new \Exception("votes did not update");
        }

        return $updatedConnection->ToArray();
    }

    function AdjustConnectionDirectionAccordingToVotes($param) {
        
    }

    function CreateHandItemFormDBNode($neoNode) {
        return new HandItem($neoNode->id(), $neoNode->getProperty('name'));
    }

    public function GetAllConnections() {
        $query = '
MATCH (a:hand)-[w:IS_STRONGER_THAN]->(b:hand)
RETURN a, b, w.beginVotes AS beginVotes, w.endVotes AS endVotes
            ';
        $results = $this->neo4jDB->run($query);
        $connections = [];
        foreach ($results as $result) {
            // Returns a Node
            $aNode = $this->CreateHandItemFormDBNode($result->get('a'));
            $bNode = $this->CreateHandItemFormDBNode($result->get('b'));


            $connection = new HandsConnection(
                    $aNode,
                    $bNode,
                    $result->get(self::VOTES_BEGIN),
                    $result->get(self::VOTES_END)
            );
            $connections[] = $connection->toArray();
        }
        return $connections;
    }

    public function GetConnection($id1, $id2) {
        $query = '
MATCH (a:hand)-[w:IS_STRONGER_THAN]-(b:hand)
WHERE id(a)=' . $id1 . ' and id(b) = ' . $id2 . '
RETURN a, b, w.beginVotes AS beginVotes, w.endVotes AS endVotes
            ';
        /*  \Tracy\Debugger::dump($query);
          die; */
        $results = $this->neo4jDB->run($query);
        if (count($results) == 0) {
            return ["error" => "no connection"];
        }
        $result = $results->first();

        $aNode = $this->CreateHandItemFormDBNode($result->get('a'));
        $bNode = $this->CreateHandItemFormDBNode($result->get('b'));


        $connection = new HandsConnection(
                $aNode,
                $bNode,
                $result->get(self::VOTES_BEGIN),
                $result->get(self::VOTES_END)
        );

        return $connection->ToArray();
    }

    function NodesDoExist($idA, $idB) {
        $query = '
MATCH (a), (b) WHERE id(a)=' . $idA . ' AND id(b)=' . $idA . ' RETURN true
            ';
        $results = $this->neo4jDB->run($query);
        if (count($results) == 0) {
            return false;
        } else {
            return true;
        }
    }

    function GetWinnerStateJson($winnerState, $winnerId) {
        return [
            "winnerState" => $winnerState,
            "winnerId" => $winnerId,
        ];
    }

    public function EvaluateFight($AIHandId, $userHandId) {
        /* \Tracy\Debugger::dump($AIHandId);
          \Tracy\Debugger::dump($userHandId);
          die; */

        if ($AIHandId == $userHandId) {
            return $this->GetWinnerStateJson(self::WINNER_STATE_NOONE, $AIHandId);
        }
        if (!$this->NodesDoExist($AIHandId, $userHandId)) {
            throw new \Exception("nodes cant fight - dont exist " . $AIHandId . " " . $userHandId);
        }

        $handConnection = $this->GetVotesForTwoHands($AIHandId, $userHandId);


        if ($handConnection->NoVotes()) {
            if (mt_rand(0, 1) == 1) {
                $handNodes = $this->AddRelationBetweenHands($AIHandId, $userHandId);
                $winnerId = $handNodes[0]->id;
                return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $winnerId);
            } else {
                $handNodes = $this->AddRelationBetweenHands($userHandId, $AIHandId);
                $winnerId = $handNodes[0]->id;
                return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $winnerId);
            }
        } else {

            if ($handConnection->beginVotes == $handConnection->endVotes) {
                return $this->GetWinnerStateJson(self::WINNER_STATE_NOONE, $AIHandId);
            }

            if ($handConnection->beginVotes > $handConnection->endVotes) {
                if ($handConnection->beginHand->id == $AIHandId) {
                    /*
                      \Tracy\Debugger::dump("beginVotes>endVotes - beginHand=AIHandId");
                      \Tracy\Debugger::dump($handConnection);
                      die;

                     */
                    return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $AIHandId);
                } else {
                    /*
                      \Tracy\Debugger::dump("beginVotes>endVotes - beginHand!=AIHandId");
                      \Tracy\Debugger::dump($handConnection);
                      die;
                     * 
                     */
                    return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $userHandId);
                }
            } else { // end is wiiner
                if ($handConnection->endHand->id == $AIHandId) {
                    /*
                      \Tracy\Debugger::dump("beginVotes<endVotes - endHand=AIHandId");
                      \Tracy\Debugger::dump($handConnection);
                      die;
                     * 
                     */
                    return $this->GetWinnerStateJson(self::WINNER_STATE_AI, $AIHandId);
                } else {
                    /*
                      \Tracy\Debugger::dump("beginVotes<endVotes - endHand!=AIHandId");
                      \Tracy\Debugger::dump($handConnection);
                      die;
                     */
                    return $this->GetWinnerStateJson(self::WINNER_STATE_USER, $userHandId);
                }
            }
        }
    }

    public function AddHandRandomly() {
        $results = $this->neo4jDB->run('MATCH (n) RETURN n');

        $nodeIds = [];
        foreach ($results as $result) {
            // Returns a Node
            $node = $result->get('n');
            $nodeIds[] = $node->id();
        }

        $randHandId = $this->GetRandomElementFromArray($nodeIds);
        /* \Tracy\Debugger::dump($randHandId);
          die; */

        $name = ucfirst($this->readable_random_string(mt_rand(4, 8)));
        $this->AddNewHandWithConnection($name, $randHandId);
        return new HandItem($randHandId, $name);
    }

    public function GetAIRandomHandId($excludeId = -1) {
        $list = $this->GetList();
        if ($excludeId != -1) {
            unset($list[$excludeId]);
        }
        $randKey = array_rand($list);
        return $list[$randKey];
    }

    public function AddRandomFight() {
        $results = $this->neo4jDB->run('
MATCH (a:hand), (b:hand)
WHERE NOT (a)-[:' . self::BASE_REL_TYPE . ']-(b:hand) AND a<>b
WITH a, b
ORDER BY rand()
LIMIT 1
CREATE (a)-[r:' . self::BASE_REL_TYPE . '{' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b)
RETURN   a,b ');
        if (count($results) == 0) {
            die("cant add new fight, everything connected");
        }
        $hands = [];
        foreach ($results as $result) {
            // Returns a Node
            $node = $result->get('a');
            $hands[] = $this->CreateHandItemFormDBNode($node); // new HandItem($node->id(), $node->getProperty('name'));
            $node = $result->get('b');
            $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));
        }
        return $hands;
    }

    function AddRelationBetweenHands($strongerId, $weakerId) {
        $results = $this->neo4jDB->run('
MATCH (a:hand), (b:hand)
WHERE NOT (a)-[:' . self::BASE_REL_TYPE . ']-(b:hand) AND a<>b AND id(a) = ' . $strongerId . ' AND id(b) = ' . $weakerId . '
CREATE (a)-[r:' . self::BASE_REL_TYPE . '{' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b)
RETURN   a,b ');
        if (count($results) == 0) {
            die("those two already have connection");
        }
        $hands = [];
        $result = $results->first();

        // Returns a Node
        $node = $result->get('a');
        $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));
        $node = $result->get('b');
        $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));

        return $hands;
    }

    function createSearchName($name) {
        return trim($name);
    }

    public function AddHand($name) {
        $searchName = $this->createSearchName($name);
        $query = '
            MATCH (a:hand) 
            WHERE a.searchName = "' . $searchName . '"
            RETURN a
        ';
        $results = $this->neo4jDB->run($query);
        if (count($results) > 0) {
            throw new \Exception("this hand already exists");
        }

        $query = '
            CREATE (a:hand{name: "' . $name . '", searchName:"' . $searchName . '"}) 
            RETURN a
        ';
        /* \Tracy\Debugger::dump($query);
          die; */
        $results = $this->neo4jDB->run($query);
        $this->RemoveCachedList();
        $hands = [];
        $result = $results->first();

        $node = $result->get('a');
        $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));

        return $hands;
    }

    public function AddNewHandWithConnection($name, $againstId) {
        $whichIsStronger;

        if (mt_rand(0, 1) == 1) {
            $whichIsStronger = '(a)-[w:' . self::BASE_REL_TYPE . '{' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(b)';
        } else {
            $whichIsStronger = '(b)-[w:' . self::BASE_REL_TYPE . '{' . self::VOTES_BEGIN . ': 1, ' . self::VOTES_END . ': 0}]->(a)';
        }

        $query = '
            MATCH (a:hand) 
            WHERE id(a) = ' . $againstId . '
            CREATE (b:hand{name: "' . $name . '"}) 
            CREATE ' . $whichIsStronger . ' 
            RETURN a, b
        ';
        /* \Tracy\Debugger::dump($query);
          die; */
        $results = $this->neo4jDB->run($query);
        $this->RemoveCachedList();
        $hands = [];
        foreach ($results as $result) {

            $node = $result->get('a');
            $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));
            $node = $result->get('b');
            $hands[] = $this->CreateHandItemFormDBNode($node); //new HandItem($node->id(), $node->getProperty('name'));
        }
        return $hands;

        /* foreach ($results as $result) {
          // Returns a Node
          $node = $result->get('n');
          \Tracy\Debugger::dump($node);
          echo $node->id(); //  getAttribute('id');
          echo $node->getProperty('name');
          } */
    }

}
