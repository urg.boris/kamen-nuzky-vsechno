<?php

/**
 * @author Chris Zuber
 * @license http://opensource.org/licenses/GPL-3.0 GNU General Public License, version 3 (GPL-3.0)
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace RandomImageGenerator;

final class RandomImageGenerator {

    private $Shapes;

    /**
     * Image width
     * @var integer
     */
    private $X = 1920;

    /**
     * Image height
     * @var integer
     */
    private $Y = 1200;

    /**
     * Number of shapes to draw
     * @var integer
     */
    private $SHAPES = 500;

    /**
     * Various available styles for arcs
     * @var Arary
     */
    const ARC_STYLES = [
        IMG_ARC_PIE,
        IMG_ARC_CHORD,
        IMG_ARC_EDGED,
        IMG_ARC_NOFILL
    ];


    public function __construct($X, $Y, $Shapes = 10) {
        $this->X = $X;
        $this->Y = $Y;
        $this->Shapes = $Shapes;
    }
    
    public function GenerateAndSave($filePath) {
        $im = imagecreatetruecolor($this->X, $this->Y);
        for ($i = 0; $i < $this->SHAPES; $i++) {
            $this->random_arc($im);
          /*  switch (random_int(1, 2)) {
                case 1:
                    $this->random_arc($im);
                    break;
                case 2:
                    $this->random_polygon($im);
                    break;
            }*/
        }
        imagepng($im, $filePath);
        imagedestroy($im);
    }
/*
 * $file = 'semitransparent.png'; // path to png image
$img = imagecreatefrompng($file); // open image
imagealphablending($img, true); // setting alpha blending on
imagesavealpha($img, true); // save alphablending setting (important)
 */

    /**
     * Create a random polygon with number of points between 0 & $max_pts
     * @param  resource  $im      The image reource
     * @param  integer   $max_pts Max number of point to use
     * @return void
     */
    function random_polygon($im, Int $max_pts = 20) {
        $color = imagecolorallocatealpha($im, ...$this->random_color_alpha());
        $pts = $this->random_pts(random_int(3, $max_pts));
        $num = count($pts) / 2;
        imagefilledpolygon($im, $pts, $num, $color);
    }

    /**
     * Creates a random arc of a random color
     * @param  resource $im The image resource
     * @return void
     */
    function random_arc($im) {
        $cx = random_int(0, $this->X);
        $cy = random_int(0, $this->Y);
        $w = random_int(0, $this->X);
        $h = random_int(0, $this->Y);
        $s = random_int(0, 360);
        $e = random_int(0, 360);
        $col = imagecolorallocatealpha($im, ...$this->random_color_alpha());
        $style = self::ARC_STYLES[random_int(0, 3)];
        imagefilledarc($im, $cx, $cy, $w, $h, $s, $e, $col, $style);
    }

    /**
     * Draws a randomly colored pixel at a random location
     * @param  resource $im The image resource
     * @return void
     */
    function random_pixel($im) {
        $color = imagecolorallocate($im, ...$this->random_color_alpha());
        $x = random_int(0, $this->X);
        $y = random_int(0, $this->Y);
        imagesetpixel($im, $x, $y, $color);
    }

    /**
     * Generates an array of random alpha color values.
     * @return Array [r, g, b, a]
     */
    function random_color_alpha(): Array {
        return [
            random_int(0, 255),
            random_int(0, 255),
            random_int(0, 255),
            random_int(0, 127)
        ];
    }

    /**
     * Generates an array of random color values
     * @return Array [r, g, b]
     */
    function random_color(): Array {
        return [random_int(0, 255), random_int(0, 255), random_int(0, 255)];
    }

    /**
     * Generates a set of random points for a polygon [x1,y1, x2,y2,...]
     * @param  integer $length Number of sets of points to generate
     * @return Array           The resulting array of points
     */
    function random_pts($length): Array {
        $pts = [];
        for ($n = 0; $n < $length; $n++) {
            $pts[] = random_int(0, $this->X);
            $pts[] = random_int(0, $this->Y);
        }
        return $pts;
    }


}




