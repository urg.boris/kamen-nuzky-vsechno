import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "./App.css";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";

function BaseContainer(props) {
  return (
    <div className="App">
      <header className="App-header">{props.content}</header>
    </div>
  );
}

export default BaseContainer;

