import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "./App.css";

import BaseContainer from "./Base";
import BaseConfig from "./config.json";
import { Howl, Howler } from "howler";

const CONNECT_TO_DB = true;
const BASE_API_URI = process.env.REACT_APP_BASE_API_URI;
const BASE_URI = process.env.REACT_APP_BASE_URI;

const FightState = {
  LOADING: "loading",
  COUNTDOWN: "countdown",
  USER_CHOOSING: "userChosing",
  USER_CREATING_HAND: "newHand",
  USER_ATTACKED: "userAttacked",
  FIGHTING: "fighting",
  RESULT: "result",
  FEEDBACK: "feedback",
};

const WinnerState = {
  BEFORE: "before",
  AI: "ai",
  USER: "user",
  NOONE: "noone",
};

const BaseSounds = {
  EVEN: CreateSound("base/even.mp3"),
  WIN: CreateSound("base/win.mp3"),
  FAIL: CreateSound("base/fail.mp3"),
  VERSUS: CreateSound("base/versus.mp3"),
};

function HandOption(props) {
  return <h1>props.id - props.name</h1>;
}

function CreateSound(fileName, format = "mp3") {
 // console.log("BASE_URI");
  //console.log(BASE_URI);
  return new Howl({
    src: [BASE_URI+"sound/" + fileName],
    format: [format],
    autoplay: false,
    loop: false,
    volume: 1,
    html5: true,
  });
}

class ChainedSound {
  constructor(howlSounds, onChainEnd) {
    //console.log("sounds count=" + howlSounds.length);
    this.howlSounds = howlSounds;
    this.onChainEnd = onChainEnd;
    this.chainPartsLoaded = 0;
    this.chainPartsTotal = howlSounds.length;
    this.PlayChainedSound(onChainEnd);
  }

  PlayChainedSound(onChainEnd) {
    // console.log("chaining"+howlSounds.length);
    for (let i = 0; i < this.howlSounds.length; i++) {
      let onEndCallback = null;
      //console.log("i"+i);
      //console.log(howlSounds[i]._src);

      if (i + 1 == this.howlSounds.length) {
        onEndCallback = onChainEnd;
      } else {
        onEndCallback = () => PlaySound(this.howlSounds[i + 1]);
      }
      //console.log(howlSounds[i].state());
      if (this.howlSounds[i].state() == "loaded") {
        //console.log(i+" already loaded");
        this.howlSounds[i].on("end", onEndCallback);
        this.onChainPartLoaded(i);
      } else {
        this.howlSounds[i].on("end", onEndCallback);
        this.howlSounds[i].once("loaderror", this.onChainSoundError.bind(this));
        this.howlSounds[i].once("load", this.onChainPartLoaded.bind(this, i));
      }
    }
  }

  stopAllSounds() {
    for (let i = 0; i < this.howlSounds.length; i++) {
      if (this.howlSounds[i] != null) {
        this.howlSounds[i].stop();
        this.howlSounds[i] = null;
      }
    }
    this.howlSounds = [];
  }

  onChainSoundError(e, msg) {
  //  console.log("Unable to load file:  | error message : " + msg);
   // console.log("First argument error " + e);
    setTimeout(this.onChainEnd.bind(this), 4000);
  }

  onChainPartLoaded(i) {
    this.chainPartsLoaded++;
    //console.log(i+"  " +this.chainPartsLoaded+" loaded ");
    if (this.chainPartsLoaded >= this.chainPartsTotal) {
      //console.log("downloaded all");
      PlaySound(this.howlSounds[0]);
    }
  }
}
let sound = null;

function StopSound() {
 // console.log("stop sound");
 // console.log(sound._src);
  if (sound != null) {
    sound.stop();
    sound = null;
  }
}

function PlaySound(howlSound) {
  if (howlSound != null) {
   // console.log("play sound");
   // console.log(howlSound._src);
    howlSound.stop();
    howlSound.play();
    sound = howlSound;
  } else {
   // console.log("sound is null");
  }
}
/*
function onChainSoundError(e, msg) {
  console.log("Unable to load file:  | error message : " + msg);
  console.log("First argument error " + e);
  setTimeout(this.endSound.bind(this), 4000);
}

let chainPartsLoaded = 0;
let chainPartsTotal = 0;
let firstHowlSounds = null;
let onChainedSoundEnd = function onChainPartLoaded() {
  chainPartsLoaded++;

  if (chainPartsLoaded >= chainPartsTotal) {
    console.log("downloaded all");
    firstHowlSounds.play();
  }
};

function PlayChainedSound(howlSounds, onChainEnd) {
  chainPartsLoaded = 0;
  chainPartsTotal = howlSounds.length;
  firstHowlSounds = howlSounds[0];
  for (let i = 0; i < howlSounds.length; i++) {
    let onEndCallback = null;
    //console.log("i"+i);
    //console.log(howlSounds[i]);

    if (i + 1 == howlSounds.length) {
      onEndCallback = onChainEnd;
    } else {
      onEndCallback = () => PlaySound(howlSounds[i + 1]);
    }

    //howlSounds[i].on('end', onEndCallback);
    howlSounds[i].once("loaderror", onChainSoundError.bind(this));
    howlSounds[i].once("load", onChainPartLoaded(howlSounds.length));
    howlSounds[i].on("end", onEndCallback);
  }
}
*/
function CreateImage(url, width, height) {
  return (
    <img
      src={url}
      style={{ width: width + "px", height: height + "px", objectFit: "cover" }}
    />
  );
}

class Hand {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

function LoadingScene() {
  return <BaseContainer content={<div>loading...</div>} />;
}



class ErrorScene extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
  }

  render() {
    return (
      <BaseContainer
        content={
          <div>
            <div>Error - {this.props.errorMsg}</div>
            <div>
              <a href="/fight">zpět k souboji</a>
            </div>
          </div>
        }
      />
    );
  }
}
class CountdownScene extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      afterAnim: props.afterAnim,
    };
  }

  componentDidMount() {
    setTimeout(this.state.afterAnim.bind(this), 0);
  }
  /*
  onEnd() {
    console.log("dasdsadsadsadasdsadsadsad");
  }*/
  render() {
    return <BaseContainer content={<div>....hloubám...</div>} />;
  }
}

class UserChoosingScreen extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      handOptionArray: Object.values(props.handOptions),
      handOptions: props.handOptions,
      handleNewHand: props.handleNewHand,
      addingNewHand: props.addingNewHand,
      searchHandStringStrip: "",
      searchHandStringValue: "",
    };
  }

  state = {
    searchHandStringStrip: "",
    searchHandStringValue: "",
    searchHandOptions: null,
  };

  removeDiacrit(query) {
    let searchHandString = query
    .normalize("NFD")
    .replace(/\p{Diacritic}/gu, "");
    return searchHandString;
  }

  handleInputChange = (event) => {
    const searchHandStringValue = event.target.value;
    let searchHandStringStrip = this.removeDiacrit(searchHandStringValue);
    this.setState({ 
      searchHandStringStrip: searchHandStringStrip,
      searchHandStringValue:searchHandStringValue
     });
    //console.log(searchHandString);
    /*
    this.setState(prevState => {
      const searchHandOptions = prevState.data.filter(element => {
        return element.name.toLowerCase().includes(query.toLowerCase());
      });

      return {
        query,
        filteredData
      };
    });*/
  };

  afterInserted = (newHand) => {
    //onHandSelected()
    //console.log("after insert");
    //console.log(newHand);
    this.props.onUserCreatedNewHand(newHand);
  };
  
  onSelectedHandOption = (event) => {
    let handId = parseInt(event.target.value);
   // console.log(event.target);
    this.props.onUserSelectedHandId(handId);
  };

  onSelectedSearchHandOption = (handId, event) => {
    event.preventDefault();

    this.setState({ newName: event.target.innerText });
    this.props.onUserSelectedHandId(handId, event.target.innerText);
  };

  /*
  state = {
    handOptions: [],
    AIHand: [],
    userHand: null,
    handleClick: null,
  };*/

  render() {
    let objAsArray = this.state.handOptions;
    //let THIS = this;
    let handsListOptions = Object.keys(objAsArray).map(function (key) {
      return (
        <option value={objAsArray[key].id} key={key}>
          {objAsArray[key].id} - {objAsArray[key].name}
        </option>
      );
    });

    if (this.props.addingNewHand == true) {
      return (
        <BaseContainer
          content={
            <div>
              <NewHandForm
                newName={this.state.newName}
                afterInserted={this.afterInserted.bind(this)}
              />
            </div>
          }
        />
      );
    } else {
      let options = this.state.handOptionArray.filter((option) => {
        if (this.state.searchHandStringValue === "") {
          return ""; //option;
        } else if (
          option.searchName.includes(this.state.searchHandStringStrip.toLowerCase())
        ) {
          // console.log(option)
          return option;
        }
      });
      if (options.length == 0 && this.state.searchHandStringValue.length > 3) {
        options[0] = { id: -666, name: this.state.searchHandStringValue };
        //  options[] = {}
      }
      return (
        <BaseContainer
          content={
            <div>
              <div>
                Hraješ kámen - nůžky - všechno proti{" "}
                <acronym title="God Of Random Numbers">GORNovi</acronym> <br />
                Zvol svojí útočnou ruku
                <br />
                <form>
                  <input
                    placeholder="vyber..."
                   // value={this.state.searchHandString}
                    onChange={this.handleInputChange}
                  />
                  {options.map((option) => (
                    <div
                      key={option.id}
                      style={{ border: "1px solid black", background: "white" }}
                    >
                      <a
                        href="#"
                        onClick={this.onSelectedSearchHandOption.bind(
                          this,
                          option.id
                        )}
                      >
                        {option.name}
                      </a>
                    </div>
                  ))}
     
                </form>
              </div>
            </div>
          }
        />
      );
      /*
                   <select onChange={this.onSelectedHandOption.bind(this)}>
                    <option value="-1"> vyber něco </option>
                    {handsListOptions}
                    <option value="-666"> !vytvořit něco jiného! </option>
                  </select>
       */
    }
  }
}

class UserAttackedScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight,
    };
   // console.log("newround");
   // console.log(this.state);
    this.userHandSound = null;
    this.aiHandSound = null;
    this.chainedSound = null;
  }

  componentDidMount() {
    this.userHandSound = new Howl({
      src: [BASE_URI+"sound/" + this.state.userHand.id + ".mp3"],
      format: ["mp3"],
      autoplay: false,
      loop: false,
      volume: 1,
      html5: true,
    });
    this.aiHandSound = new Howl({
      src: [BASE_URI+"sound/" + this.state.aiHand.id + ".mp3"],
      format: ["mp3"],
      autoplay: false,
      loop: false,
      volume: 1,
      html5: true,
    });
   // console.log(this.userHandSound);
    //console.log(this.aiHandSound);
    this.chainedSound = new ChainedSound(
      [this.userHandSound, BaseSounds.VERSUS, this.aiHandSound],
      this.endSound.bind(this)
    );
    /*
    PlayChainedSound(
      [this.userHandSound, BaseSounds.VERSUS, this.aiHandSound],
      this.endSound.bind(this)
    );*/
    //this.startSound();
  }

  onSoundError(e, msg) {
    console.log("Unable to load file:  | error message : " + msg);
    console.log("First argument error " + e);
    setTimeout(this.endSound.bind(this), 4000);
  }
  /*
  startSound() {
    setTimeout(this.endSound.bind(this), 4000);
    this.sound.play();
  }*/

  endSound() {
    // this.userHandSound.stop();
   // console.log("ending");
    this.chainedSound.stopAllSounds();
    this.state.afterFight();
  }

  componentWillUnmount() {
    this.chainedSound.stopAllSounds();
  }

  render() {
    var img1URL = BASE_URI + "images/" + this.state.userHand.id + ".png";
    var img2URL = BASE_URI + "images/" + this.state.aiHand.id + ".png";
    //ine-height: 150px;
    //height: 150px
    return (
      <BaseContainer
        content={
          <div>
            <div style={{ display: "flex", alignItems: "center" }}>
              {CreateImage(img1URL, 100, 100)}
              {"           "}
              <div style={{ paddingLeft: 50 + "px" }}>
                {this.state.userHand.name}
              </div>
            </div>
            <br />
            <div>VERSUS</div>
            <br />
            <div style={{ display: "flex", alignItems: "center" }}>
              {CreateImage(img2URL, 100, 100)}
              {"           "}
              <div style={{ paddingLeft: 50 + "px" }}>
                {this.state.aiHand.name}
              </div>
            </div>
          </div>
        }
      />
    );
  }
}

class NewHandForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      alreadyProcessing: false,
      error: null,
      //afterInserted: props.afterInserted,
      //againstId: props.againstId,
      handData: {
        name: this.props.newName,
        image: null,
        sound: null,
      },
      /*userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight*/
    };
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
  }

  componentDidMount() {
    console.log("mount")
    //this.updateNameValue();
  }




  /*
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
   // this.setState({ data: nextProps.data });  
  }*/

  addNewHand(event) {
    event.preventDefault();
    this.setState({ alreadyProcessing: true });
    let formData = new FormData();
    formData.append("name", this.state.handData.name);
    if (this.state.handData.image != null) {
      formData.append("image", this.state.handData.image, "image.jpg");
    }

    if (this.state.handData.sound != null) {
      formData.append("sound", this.state.handData.sound, "sound.wav");
    }

    let THIS = this;
console.log(this.state.handData.name);
    //data.append("name", this.state.handData.name);
    axios
      .post(
        BASE_API_URI + "add-hand/",
        formData,

        {
          headers: {
            "Content-Type": "multipart/form-data",
            crossdomain: true,
          },
        }
      )
      .then((response) => {
        //console.log(response);
        let data = response.data;
        let newHand = new Hand(data.id, data.name);
        //console.log("newHand");
        //console.log(newHand);



        this.props.afterInserted(newHand);
      })
      .catch(function (error) {
        //todo
        console.log(error);
        THIS.setState({ error: error });
      });
  }

  updateNameValue(evt) {
    //console.log(evt);
    const val = evt.target.value;
    var handData = this.state.handData;
    handData.name = val;
    this.setState({
      handData: handData,
    });
  }
  updateImageValue(evt) {
    //console.log(evt.target.files[0]);
    var handData = this.state.handData;
    handData.image = evt.target.files[0];
    this.setState({
      handData: handData,
    });
  }
  updateSoundValue(evt) {
    //console.log(evt.target.files[0]);
    var handData = this.state.handData;
    handData.sound = evt.target.files[0];
    this.setState({
      handData: handData,
    });
  }

  render() {
    if (this.state.error != null) {
      return <ErrorScene errorMsg={this.state.error.message} />;
    } else {
      if (this.state.alreadyProcessing) {
        return <div>processing...</div>;
      } else {
        return (
          <form encType="multipart/form-data">
            <div>
              To tu ještě nemáme! přidej obrázek a zvuk nové útočné ruky. <br />
              nebo ne. my to nějak uvaříme. nebo taky ne.<br />
              <label>
                Jméno:
                <input
                  name="name"
                  disabled={true}
                  value={this.props.newName}
                  onChange={this.updateNameValue.bind(this)}
                />
              </label>
            </div>
            <div>
              <label>
                Obrázek:
                <input
                  type="file"
                  name="image"
                  accept=".png, image/png"
                  onChange={this.updateImageValue.bind(this)}
                />
              </label>
            </div>
            <div>
              <label>
                Zvuk:
                <input
                  type="file"
                  name="sound"
                  accept=".mp3, audio/mp3"
                  onChange={this.updateSoundValue.bind(this)}
                />
              </label>
            </div>
            <input
              type="submit"
              value="Submit"
              onClick={this.addNewHand.bind(this)}
            />
            <div>
              <a href="/fight">zpět k souboji</a>
            </div>
          </form>

        );
      }
    }
  }
}

class FightingScene extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      /*userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight*/
    };
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
  }

  render() {
    return <BaseContainer content={<div>fighting</div>} />;
  }
}

class ResultScreen extends React.Component {
  constructor(props) {
    super(props);
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
    this.state = {
      winnerState: props.winnerState,
      onPlayAgain: props.onPlayAgain,
      userHand: props.userHand,
      aiHand: props.aiHand,
      onBlameProccess: props.onBlameProccess,
      onIncrementVote: props.onIncrementVote,
    };
    this.sound = null;
  }

  /*
  state = {
    handOptions: [],
    AIHand: [],
    userHand: null,
    handleClick: null,
  };*/

  incrementVote(voteId, connectionId, evt) {
    evt.preventDefault();
    this.state.onBlameProccess();
    //console.log("onincrementVote");
    //console.log(voteId, connectionId);

    axios
      .get(
        BASE_API_URI +
          "increment-vote?forNodeId=" +
          voteId +
          "&connectedToNode=" +
          connectionId,
        {
          headers: {
            crossdomain: true,
          },
        }
      )
      .then((res) => {
        const connection = res.data;
        //console.log("voting");
        //console.log(connection);
        this.state.onIncrementVote();
        /*  this.setState({
          currentFightState: FightState.FEEDBACK,
        });*/
        //console.log(handOptions);
      })
      .catch((err) => {
        this.setState({ error: err });
        console.log(err);
      });
  }

  componentWillUnmount() {
    StopSound();
  }

  render() {
    //console.log("winnerState switch = " + this.state.winnerState);
    let resultText = "";
    let AIWinned = this.state.winnerState == WinnerState.AI;
    let loosingHandId;
    let winningHandId;
    let playAgain = (
      <a href="#" onClick={this.state.onPlayAgain}>
        <div>Hrát znova</div>
      </a>
    );
    switch (this.state.winnerState) {
      /*
            <div>
        <a href={"/blame/blame/" + loosingHandId + "/" + winningHandId}>
          blame
        </a>
      </div>
      */
      case WinnerState.AI:
        PlaySound(BaseSounds.FAIL);
        resultText = (
          <div>
            <acronym title="God Of Random Numbers">GORN</acronym> vyhrál.{" "}
            {this.state.userHand.name} je bohužel méně než{" "}
            {this.state.aiHand.name}
            <br />
            <a
              href="#"
              onClick={this.incrementVote.bind(
                this,
                this.state.userHand.id,
                this.state.aiHand.id
              )}
            >
              ale ja si myslim ze {this.state.userHand.name} je vic nez{" "}
              {this.state.aiHand.name}
            </a>
            <br />
            {playAgain}
          </div>
        );
        loosingHandId = this.state.userHand.id;
        winningHandId = this.state.aiHand.id;
        break;
      case WinnerState.USER:
        PlaySound(BaseSounds.WIN);
        resultText = (
          <div>
            vyhráls! {this.state.userHand.name} je opravdu více než{" "}
            {this.state.aiHand.name}
            <br />
            {playAgain}
          </div>
        );

        loosingHandId = this.state.aiHand.id;
        winningHandId = this.state.userHand.id;
        break;
      case WinnerState.NOONE:
        PlaySound(BaseSounds.EVEN);
        resultText = (
          <div>
            remíza - oba ste vyhráli! <br />
            {this.state.userHand.name} je stejne jako {this.state.aiHand.name}
            <br />
            {playAgain}
          </div>
        );
        loosingHandId = this.state.aiHand.id;
        winningHandId = this.state.aiHand.id;
        break;
      default:
        resultText = "neco fuj";
    }

    return (
      <BaseContainer
        content={
          <div>
            <div>{resultText}</div>
          </div>
        }
      />
    );
  }
}

class Fight extends React.Component {
  constructor(props) {
    super(props);

    //this.handleClick = this.handleClick.bind(this);
  }

  state = {
    handOptions: [],
    aiHand: [],
    userHand: null,
    currentFightState: FightState.LOADING,
    currentWinnerState: WinnerState.BEFORE,
    newHand: null,
    addingNewHand: false,
  };

  afterUserSelected = () => {
    this.setState({
      currentFightState: FightState.USER_ATTACKED,
    });
  };

  onPlayAgain = () => {
    this.setState({
      currentFightState: FightState.LOADING,
      currentWinnerState: WinnerState.BEFORE,
      addingNewHand: false,
    });
    this.loadData();
  };

  afterCountDown = () => {
    this.setState({
      currentFightState: FightState.USER_CHOOSING,
    });
  };

  handleNewHand = (name) => {
    /*console.log("handleNewHand");
    console.log(name);*/
    this.setState({ newHand: new Hand(-1, name) });
  };

  afterFight = () => {
    this.evaluateFight(this.state.aiHand.id, this.state.userHand.id);
  };

  evaluateFight(AIHandId, userHandId) {
    /*console.log("evaluateFight");
    console.log("you=" + userHandId);
    console.log("ai=" + AIHandId);*/
    if (AIHandId == userHandId) {
      this.state.currentWinnerState = WinnerState.NOONE;
      this.setState({
        currentFightState: FightState.RESULT,
      });
    } else {
      axios
        .get(
          BASE_API_URI + "fight?userHand=" + userHandId + "&aiHand=" + AIHandId,
          {
            headers: {
              crossdomain: true,
            },
          }
        )
        .then((res) => {
          const winnerState = res.data["winnerState"];

          this.setState({ error: null });

          this.setState({ currentWinnerState: winnerState });
          this.setState({
            currentFightState: FightState.RESULT,
          });
          //console.log("aft axios state " + this.state.currentWinnerState);
        })
        .catch((err) => {
          this.setState({ error: err });
          console.log(err);
        });
    }
  }

  loadData() {
    if (CONNECT_TO_DB) {
      axios
        .get(BASE_API_URI + "list", {
          headers: {
            crossdomain: true,
          },
        })
        .then((res) => {
          //const handOptions = res.data;
          handOptions = {};
          var key = Object.keys(res.data);
          for (let index = 0; index < key.length; index++) {
            const element = res.data[index];
            handOptions[element.id] = element;
          }
          /*
          Object.keys(res.data).forEach(element, index => {
            console.log(element);
            handOptions[element.id] = res.data[index];
          });*/
          //console.log("OPTION");
          //console.log(handOptions);
          this.setState({ error: null });
          this.setState({ handOptions });
          //console.log(BASE_API_URI);
          axios
            .get(BASE_API_URI + "get-random-hand", {
              headers: {
                crossdomain: true,
              },
            })
            .then((res) => {
              const AIHand = new Hand(res.data.id, res.data.name);

              this.setState({ error: null });
              this.setState({ currentFightState: FightState.COUNTDOWN });
              this.setState({ aiHand: AIHand });

              //console.log(AIHand);
            })
            .catch((err) => {
              this.setState({ error: err });
              console.log(err);
            });

          //console.log(handOptions);
        })
        .catch((err) => {
          this.setState({ error: err });
          console.log(err);
        });
    } else {
      var handOptions = [
        { id: 1, name: "lala1" },
        { id: 2, name: "lala2" },
      ];
      this.setState({ handOptions });
      const AIHand = new Hand(handOptions[0].id, handOptions[0].name);
      this.setState({ AIHand: AIHand });
      this.setState({ currentFightState: FightState.COUNTDOWN });
    }
  }

  componentDidMount() {
    this.loadData();
  }

  onUserCreatedNewHand = (newHand) => {
    this.setState({ userHand: newHand });
    // console.log("handleClick");
    // console.log("user hand:", userHandId);
    // console.log("ai hand:", this.state.AIHand.id);
    this.afterUserSelected();
  };

  onUserSelectedHandId = (handId, name) => {
    // console.log("selected handId" + handId);

    let selectedHandId = parseInt(handId);
    //console.log("selectedHandId");
    //console.log(selectedHandId);
    switch (selectedHandId) {
      case -1:
        return;
        break;
      case -666:
        //console.log("new hand start");
        this.setState({ addingNewHand: true });
        this.setState({ name: name });
        //console.log(this.state.addingNewHand);
        break;
      default:
        let userHandData = this.state.handOptions[handId];
        let userHand = new Hand(userHandData.id, userHandData.name);

        this.setState({ userHand: userHand });
        // console.log("handleClick");
        // console.log("user hand:", userHandId);
        // console.log("ai hand:", this.state.AIHand.id);
        this.afterUserSelected();
    }
  };

  onBlameProccess() {
    this.setState({
      currentFightState: FightState.LOADING,
    });
  }

  onIncrementVote() {
    this.setState({
      currentFightState: FightState.FEEDBACK,
    });
  }

  /* componentDidUpdate() { 
    console.log(this.state);
    }*/

  render() {
    if (this.state.error) {
      // console.log("dasdsads");
      return <ErrorScene errorMsg={this.state.error.message} />;
    } else {
      switch (this.state.currentFightState) {
        case FightState.LOADING:
          return <LoadingScene />;
          break;
        case FightState.FEEDBACK:
          return (
            <BaseContainer
              content={
                <div>
                  Děkujem za názor.
                  <br />
                  <a href="/fight">Hrát znova</a>
                </div>
              }
            />
          );
          break;
        case FightState.COUNTDOWN:
          return <CountdownScene afterAnim={this.afterCountDown} />;
          break;
        case FightState.USER_CHOOSING:
          //          console.log("FightState.USER_CHOOSING"+this.state.addingNewHand);
          return (
            <UserChoosingScreen
              onUserCreatedNewHand={this.onUserCreatedNewHand}
              onUserSelectedHandId={this.onUserSelectedHandId}
              handOptions={this.state.handOptions}
              handleNewHand={this.handleNewHand}
              addingNewHand={this.state.addingNewHand}
              againstId={this.state.aiHand.id}
            />
          );
          break;
        case FightState.USER_CREATING_HAND:
          return <div>USER_CREATING_HAND</div>;
          break;
        case FightState.USER_ATTACKED:
          return (
            <UserAttackedScreen
              aiHand={this.state.aiHand}
              userHand={this.state.userHand}
              afterFight={this.afterFight}
            />
          );
          break;
        case FightState.FIGHTING:
          return (
            //this.evaluateFight(this.state.aiHand.id, this.state.userHand.id);
            <FightingScene />
          );
          break;
        case FightState.RESULT:
          /*console.log(
            "this.state.currentWinnerState=" + this.state.currentWinnerState
          );*/

          return (
            <ResultScreen
              winnerState={this.state.currentWinnerState}
              onPlayAgain={this.onPlayAgain}
              userHand={this.state.userHand}
              aiHand={this.state.aiHand}
              onBlameProccess={this.onBlameProccess.bind(this)}
              onIncrementVote={this.onIncrementVote.bind(this)}
            />
          );
          break;

        default:
          return <ErrorScene errorMsg="missing state" />;
      }
    }
  }
}

export default Fight;
