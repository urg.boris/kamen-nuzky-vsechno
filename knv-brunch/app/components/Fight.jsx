import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "./App.css";

import BaseContainer from "./Base";
import BaseConfig from "./config.json";
import {Howl, Howler} from 'howler';

const CONNECT_TO_DB = true;

const FightState = {
  LOADING: "loading",
  COUNTDOWN: "countdown",
  USER_CHOOSING: "userChosing",
  USER_CREATING_HAND: "newHand",
  USER_ATTACKED: "userAttacked",
  FIGHTING: "fighting",
  RESULT: "result",
};

const WinnerState = {
  BEFORE: "before",
  AI: "ai",
  USER: "user",
  NOONE: "noone",
};

function HandOption(props) {
  return <h1>props.id - props.name</h1>;
}

class Hand {
  constructor(id, name) {
    this.id = id;
    this.name = name;
  }
}

function LoadingScene() {
  return <BaseContainer content={<div>loading...</div>} />;
}

const BASE_URI = process.env.REACT_APP_SERVER_URL;

class ErrorScene extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
  }

  render() {
    return <BaseContainer content={<div>Error - {this.props.errorMsg}</div>} />;
  }
}
class CountdownScene extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      afterAnim: props.afterAnim,
    };
  }

  componentDidMount() {
    setTimeout(this.state.afterAnim.bind(this), 0);
  }
  render() {
    return <BaseContainer content={<div>....hloubám...</div>} />;
  }
}

class UserChoosingScreen extends React.Component {
  constructor(props) {
    super(props);
    //console.log(props);
    this.state = {
      handOptions: props.handOptions,
      handleNewHand: props.handleNewHand,
      addingNewHand: props.addingNewHand,
    };
  }

  afterInserted = (newHand) => {
    //onHandSelected()
    console.log("after insert");
    console.log(newHand);
    this.props.onUserCreatedNewHand(newHand);
  };

  onSelectedHandOption = (event) => {
    let handId = parseInt(event.target.value);
    console.log("onSelectedHandOption" + handId);
    this.props.onUserSelectedHandId(handId);
  };

  /*
  state = {
    handOptions: [],
    AIHand: [],
    userHand: null,
    handleClick: null,
  };*/

  render() {
    let objAsArray = this.state.handOptions;
    //let THIS = this;
    let handsListOptions = Object.keys(objAsArray).map(function (key) {
      return (
        <option value={key} key={key}>
          {key} - {objAsArray[key].name}
        </option>
      );
    });

    if (this.props.addingNewHand == true) {
      return (
        <BaseContainer
          content={
            <div>
              <NewHandForm afterInserted={this.afterInserted.bind(this)} />
            </div>
          }
        />
      );
    } else {
      return (
        <BaseContainer
          content={
            <div>
              <div>
                Zvol svojí útočnou ruku<br/>
                <select onChange={this.onSelectedHandOption.bind(this)}>
                  <option value="-1"> vyber něco </option>
                  {handsListOptions}
                  <option value="-666"> !vytvořit něco jiného! </option>
                </select>
              </div>
            </div>
          }
        />
      );
    }
  }
}

class UserAttackedScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight,
    };
  }

  componentDidMount() {
    var sound = new Howl({
      src: ['sound/'+this.state.userHand.id+'.mp3']
    });
    
    sound.play();
    setTimeout(this.state.afterFight.bind(this), 1000);
  }

  render() {
    return (
      <BaseContainer
        content={
          <div>
            <div>
              you attacked with hand: {this.state.userHand.id} -{" "}
              {this.state.userHand.name} <br />
              ?===?
              <br />
              selected by GOD of random numbers: {this.state.aiHand.id} -
              {this.state.aiHand.name}
              <br />
            </div>
          </div>
        }
      />
    );
  }
}

class NewHandForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      alreadyProcessing: false,
      //afterInserted: props.afterInserted,
      //againstId: props.againstId,
      handData: {
        name: "",
        image: null,
        sound: null
      },
      /*userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight*/
    };
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
  }
  /*
  componentWillReceiveProps(nextProps) {
    console.log(nextProps);
   // this.setState({ data: nextProps.data });  
  }*/

  addNewHand(event) {
    event.preventDefault();
    this.setState({ alreadyProcessing: true });
    let formData = new FormData();
    formData.append("name", this.state.handData.name);
    if(this.state.handData.image!=null) {
      formData.append("image", this.state.handData.image, "image.jpg");
    }

    if(this.state.handData.sound!=null) {
      formData.append("sound", this.state.handData.sound, "sound.wav");
    }

    

    //data.append("name", this.state.handData.name);
    axios
      .post(
        BASE_URI + "add-hand/",
        formData,

        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      )
      .then((response) => {
        console.log(response);
        let data = response.data;
        let newHand = new Hand(data.id, data.name);
        console.log("newHand");
        console.log(newHand);

        this.props.afterInserted(newHand);
      })
      .catch(function (error) {
        //todo
        console.log(error);
      });

  }

  updateNameValue(evt) {
    const val = evt.target.value;
    var handData = this.state.handData;
    handData.name = val;
    this.setState({
      handData: handData,
    });
  }
  updateImageValue(evt) {
    console.log(evt.target.files[0]);
    var handData = this.state.handData;
    handData.image = evt.target.files[0];
    this.setState({
      handData: handData,
    });
  }
  updateSoundValue(evt) {
    console.log(evt.target.files[0]);
    var handData = this.state.handData;
    handData.sound = evt.target.files[0];
    this.setState({
      handData: handData,
    });
  }

  
  render() {
    if (this.state.alreadyProcessing) {
      return <div>processing...</div>;
    } else {
      return (
        <form  encType='multipart/form-data'>
          <div>
            <label>
              Name:
              <input name="name" onChange={this.updateNameValue.bind(this)} />
            </label>
          </div>
          <div>
            <label>
              Image:
              <input
                type="file"
                name="image"
                accept=".png, image/png"
                onChange={this.updateImageValue.bind(this)}
              />
            </label>
          </div>
          <div>
            <label>
              Sound:
              <input 
              type="file" 
              name="sound" 
              accept=".wav, audio/wav"  
              onChange={this.updateSoundValue.bind(this)}              
              />
            </label>
          </div>
          <input
            type="submit"
            value="Submit"
            onClick={this.addNewHand.bind(this)}
          />
          <div>
            <a href="/fight">back to fight</a>
          </div>
        </form>
      );
    }
  }
}

class FightingScene extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      /*userHand: props.userHand,
      aiHand: props.aiHand,
      afterFight: props.afterFight*/
    };
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
  }

  render() {
    return <BaseContainer content={<div>fighting</div>} />;
  }
}

class ResultScreen extends React.Component {
  constructor(props) {
    super(props);
    // console.log("ResultScreen + this.props: "+props.winnerState);
    // console.log(props.winnerState);
    this.state = {
      winnerState: props.winnerState,
      onPlayAgain: props.onPlayAgain,
      userHand: props.userHand,
      aiHand: props.aiHand,
    };
  }

  /*
  state = {
    handOptions: [],
    AIHand: [],
    userHand: null,
    handleClick: null,
  };*/

  render() {
    console.log("winnerState switch = " + this.state.winnerState);
    let resultText = "";
    switch (this.state.winnerState) {
      case WinnerState.AI:
        resultText =
          "AI vyhral - " +
          this.state.aiHand.name +
          " je opravdu vice nez " +
          this.state.userHand.name;
        break;
      case WinnerState.USER:
        resultText =
          "vyhrals! " +
          this.state.userHand.name +
          " je opravdu vice nez " +
          this.state.aiHand.name;
        break;
      case WinnerState.NOONE:
        resultText = (
          <span>
            remiza - oba ste vybrali! <br />
            {this.state.userHand.name} je stejne jako {this.state.aiHand.name}
          </span>
        );
        break;
      default:
        resultText = "neco fuj";
    }
    let again = (
      <a href="#" onClick={this.state.onPlayAgain}>
        <div>Again</div>
      </a>
    );

    let blame =
      this.state.aiHand.id != this.state.userHand.id ? (
        <div>
          <a
            href={
              "/blame/blame/" +
              this.state.aiHand.id +
              "/" +
              this.state.userHand.id
            }
          >
            blame
          </a>
        </div>
      ) : (
        ""
      );

    return (
      <BaseContainer
        content={
          <div>
            <div>{resultText}</div>
            {again}
            {blame}
          </div>
        }
      />
    );
  }
}

class Fight extends React.Component {
  constructor(props) {
    super(props);

    //this.handleClick = this.handleClick.bind(this);
  }

  state = {
    handOptions: [],
    aiHand: [],
    userHand: null,
    currentFightState: FightState.LOADING,
    currentWinnerState: WinnerState.BEFORE,
    newHand: null,
    addingNewHand: false,
  };

  afterUserSelected = () => {
    this.setState({
      currentFightState: FightState.USER_ATTACKED,
    });
  };

  onPlayAgain = () => {
    this.setState({
      currentFightState: FightState.LOADING,
      currentWinnerState: WinnerState.BEFORE,
    });
    this.loadData();
  };

  afterCountDown = () => {
    this.setState({
      currentFightState: FightState.USER_CHOOSING,
    });
  };

  handleNewHand = (name) => {
    console.log("handleNewHand");
    console.log(name);
    this.setState({ newHand: new Hand(-1, name) });
  };

  afterFight = () => {
    this.evaluateFight(this.state.aiHand.id, this.state.userHand.id);
  };

  evaluateFight(AIHandId, userHandId) {
    console.log("evaluateFight");
    console.log("you=" + userHandId);
    console.log("ai=" + AIHandId);
    if (AIHandId == userHandId) {
      this.state.currentWinnerState = WinnerState.NOONE;
      this.setState({
        currentFightState: FightState.RESULT,
      });
    } else {
      axios
        .get(
          BASE_URI + "fight?userHand=" + userHandId + "&aiHand=" + AIHandId,
          {
            headers: {
              crossdomain: true,
            },
          }
        )
        .then((res) => {
          const winnerState = res.data["winnerState"];

          this.setState({ error: null });

          this.setState({ currentWinnerState: winnerState });
          this.setState({
            currentFightState: FightState.RESULT,
          });
          console.log("aft axios state " + this.state.currentWinnerState);
        })
        .catch((err) => {
          this.setState({ error: err });
          console.log(err);
        });
    }
  }

  loadData() {
    if (CONNECT_TO_DB) {
      axios
        .get(BASE_URI + "list", {
          headers: {
            crossdomain: true,
          },
        })
        .then((res) => {
          const handOptions = res.data;
          this.setState({ error: null });
          this.setState({ handOptions });
          // console.log(handOptions);
          axios
            .get(BASE_URI + "get-random-hand", {
              headers: {
                crossdomain: true,
              },
            })
            .then((res) => {
              const AIHand = new Hand(res.data.id, res.data.name);

              this.setState({ error: null });
              this.setState({ currentFightState: FightState.COUNTDOWN });
              this.setState({ aiHand: AIHand });

              //console.log(AIHand);
            })
            .catch((err) => {
              this.setState({ error: err });
              console.log(err);
            });

          //console.log(handOptions);
        })
        .catch((err) => {
          this.setState({ error: err });
          console.log(err);
        });
    } else {
      var handOptions = [
        { id: 1, name: "lala1" },
        { id: 2, name: "lala2" },
      ];
      this.setState({ handOptions });
      const AIHand = new Hand(handOptions[0].id, handOptions[0].name);
      this.setState({ AIHand: AIHand });
      this.setState({ currentFightState: FightState.COUNTDOWN });
    }
  }

  componentDidMount() {
    this.loadData();
  }

  onUserCreatedNewHand = (newHand) => {
    this.setState({ userHand: newHand });
    // console.log("handleClick");
    // console.log("user hand:", userHandId);
    // console.log("ai hand:", this.state.AIHand.id);
    this.afterUserSelected();
  };

  onUserSelectedHandId = (handId) => {
    console.log("selected handId" + handId);

    let selectedHandId = parseInt(handId);
    switch (selectedHandId) {
      case -1:
        return;
        break;
      case -666:
        console.log("new hand start");
        this.setState({ addingNewHand: true });
        //console.log(this.state.addingNewHand);
        break;
      default:
        let userHandData = this.state.handOptions[handId];
        let userHand = new Hand(userHandData.id, userHandData.name);

        this.setState({ userHand: userHand });
        // console.log("handleClick");
        // console.log("user hand:", userHandId);
        // console.log("ai hand:", this.state.AIHand.id);
        this.afterUserSelected();
    }
  };

  /* componentDidUpdate() { 
    console.log(this.state);
    }*/

  render() {
    if (this.state.error) {
      // console.log("dasdsads");
      return <ErrorScene errorMsg={this.state.error.message} />;
    } else {
      switch (this.state.currentFightState) {
        case FightState.LOADING:
          return <LoadingScene />;
          break;
        case FightState.COUNTDOWN:
          return <CountdownScene afterAnim={this.afterCountDown} />;
          break;
        case FightState.USER_CHOOSING:
          //          console.log("FightState.USER_CHOOSING"+this.state.addingNewHand);
          return (
            <UserChoosingScreen
              onUserCreatedNewHand={this.onUserCreatedNewHand}
              onUserSelectedHandId={this.onUserSelectedHandId}
              handOptions={this.state.handOptions}
              handleNewHand={this.handleNewHand}
              addingNewHand={this.state.addingNewHand}
              againstId={this.state.aiHand.id}
            />
          );
          break;
        case FightState.USER_CREATING_HAND:
          return <div>USER_CREATING_HAND</div>;
          break;
        case FightState.USER_ATTACKED:
          return (
            <UserAttackedScreen
              aiHand={this.state.aiHand}
              userHand={this.state.userHand}
              afterFight={this.afterFight}
            />
          );
          break;
        case FightState.FIGHTING:
          return (
            //this.evaluateFight(this.state.aiHand.id, this.state.userHand.id);
            <FightingScene />
          );
          break;
        case FightState.RESULT:
          console.log(
            "this.state.currentWinnerState=" + this.state.currentWinnerState
          );

          return (
            <ResultScreen
              winnerState={this.state.currentWinnerState}
              onPlayAgain={this.onPlayAgain}
              userHand={this.state.userHand}
              aiHand={this.state.aiHand}
            />
          );
          break;

        default:
          return <ErrorScene errorMsg="missing state" />;
      }
    }
  }
}

export default Fight;
