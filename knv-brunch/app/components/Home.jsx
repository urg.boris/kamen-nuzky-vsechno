import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "./App.css";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import BaseContainer from "./Base";

function Home(props) {
  return (
    <BaseContainer
      content={
        <div>
          <Link to="/fight">fight</Link><br/>
          <Link to="/blame">blame</Link>
        </div>
      }
    />
  );
}

export default Home;
