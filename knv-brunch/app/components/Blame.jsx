import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import "./App.css";
import {
  BrowserRouter,
  Link,
  Route,
  Routes,
  useParams,
} from "react-router-dom";

import BaseContainer from "./Base";
import BaseConfig from "./config.json";



const BlameState = {
  LOADING: "loading",
  BLAME: "blame",
};

const BASE_URI = process.env.REACT_APP_SERVER_URL;

function BlameLinksScene() {
  return (
    <BaseContainer
      content={
        <div>
          <Link to="list">list</Link>
          <br />
          <Link to="blame/1/2">blame</Link>
        </div>
      }
    />
  );
}

function LoadingScene() {
  return <BaseContainer content={<div>loading...</div>} />;
}

class ErrorScene extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
  }

  render() {
    return <BaseContainer content={<div>Error - {this.props.errorMsg}</div>} />;
  }
}

class BlameListScene extends React.Component {
  constructor(props) {
    super(props);
    //console.log(this.props);
  }

  state = {
    list: null,
  };

  loadData() {
    axios
      .get(BASE_URI + "get-all-connections", {
        headers: {
          crossdomain: true,
        },
      })
      .then((res) => {
        const list = res.data;
        this.setState({ list: list });
        console.log(list);

        //console.log(handOptions);
      })
      .catch((err) => {
        this.setState({ error: err });
        console.log(err);
      });
  }
  componentDidMount() {
    this.loadData();
  }

  render() {
    if (this.state.list == null) {
      return <LoadingScene />;
    } else {
      if (this.state.error) {
        // console.log("dasdsads");
        return <ErrorScene errorMsg={this.state.error.message} />;
      } else {
        let list = this.state.list;
        let connectionsList = Object.keys(list).map(function (key) {
          return (
            <div key={key}>
              ({key}) - {list[key].beginNode.id} -{list[key].beginNode.name} (
              {list[key].beginVotes}) versus
              {list[key].endNode.id} -{list[key].endNode.name}(
              {list[key].endVotes})
            </div>
          );
        });
        return <BaseContainer content={<div>{connectionsList}</div>} />;
      }
    }
  }
}

const BlameBlameScene = () => {
  const { id1, id2 } = useParams();
  return <BlameConnectionStatus id1={id1} id2={id2} />;
};

class BlameConnectionStatus extends React.Component {
  constructor(props) {
    super(props);

    //this.incrementVote = this.incrementVote.bind(this);
    // const { id1, id2 } = useParams();
    //this.handleClick = this.handleClick.bind(this);
  }

  state = {
    beginVotes: null,
    endVotes: null,
  };

  loadData() {
    this.setState({ currentBlameState: BlameState.LOADING });
    axios
      .get(
        BASE_URI +
          "get-connection?id1=" +
          this.props.id1 +
          "&id2=" +
          this.props.id2,
        {
          headers: {
            crossdomain: true,
          },
        }
      )
      .then((res) => {
        const connection = res.data;
        if (connection.error) {
          this.setState({ error: connection.error });
        } else {
          this.setState({
            connection: connection,
            currentBlameState: BlameState.BLAME,
            beginVotes: connection.beginVotes,
            endVotes: connection.endVotes,
          });
          console.log(connection);
        }
        //console.log(handOptions);
      })
      .catch((err) => {
        this.setState({ error: err });
        console.log(err);
      });
  }
  componentDidMount() {
    this.loadData();
  }

  state = {
    currentBlameState: BlameState.LOADING,
    error: null,
  };

  incrementVote(voteId, connectionId, evt) {
    evt.preventDefault();
    this.setState({ currentBlameState: BlameState.LOADING });
    console.log("onincrementVote");
    console.log(voteId, connectionId);
    axios
      .get(
        BASE_URI +
          "increment-vote?forNodeId=" +
          voteId +
          "&connectedToNode=" +
          connectionId,
        {
          headers: {
            crossdomain: true,
          },
        }
      )
      .then((res) => {
        const connection = res.data;
        console.log("voting");
        console.log(connection);
        this.setState({
          beginVotes: connection.beginVotes,
          endVotes: connection.endVotes,
          currentBlameState: BlameState.BLAME,
        });
        //console.log(handOptions);
      })
      .catch((err) => {
        this.setState({ error: err });
        console.log(err);
      });
  }

  render() {
    if (this.state.error) {
      // console.log("dasdsads");
      return <ErrorScene errorMsg={this.state.error.message} />;
    } else {
      switch (this.state.currentBlameState) {
        case BlameState.LOADING:
          return <LoadingScene />;
          break;
        case BlameState.BLAME:
          // const { id1, id2 } = useParams();
          return (
            //onClick={this.incrementVote(this.state.connection.beginNode.id, this.state.connection.endNode.id).bind(this)}
            //onClick={this.incrementVote(this.state.connection.endNode.id, this.state.connection.beginNode.id)}
            <BaseContainer
              content={
                <div>
                  <a
                    href="#"
                    onClick={this.incrementVote.bind(
                      this,
                      this.state.connection.beginNode.id,
                      this.state.connection.endNode.id
                    )}
                  >
                    {this.state.connection.beginNode.id} -{" "}
                    {this.state.connection.beginNode.name} (
                    {this.state.beginVotes})
                  </a>
                  -
                  <a
                    href="#"
                    onClick={this.incrementVote.bind(
                      this,
                      this.state.connection.endNode.id,
                      this.state.connection.beginNode.id
                    )}
                  >
                    {this.state.connection.endNode.id} -{" "}
                    {this.state.connection.endNode.name} ({this.state.endVotes})
                  </a>
                  <div>
                    <a href="/fight">fight</a>
                  </div>
                </div>
              }
            />
          );
          break;
      }
    }
  }
}

export { BlameBlameScene, BlameLinksScene, BlameListScene };
