import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";
import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import "./App.css";
import Fight from "./Fight";
import {BlameBlameScene, BlameLinksScene, BlameListScene} from "./Blame";
import Home from "./Home";


const CONNECT_TO_DB = false;

/*
          <Route path="/narwhal">
            <Narwhal />
          </Route>
          <Route path="/whale">
            <Whale />
          </Route>

                    <Route path="/">
            <BaseContainer>
              <Link to="/fight" />
            </BaseContainer>
          </Route>
          */
function App() {
  return (
    <div className="wrapper">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} exact />
          <Route path="/fight" element={<Fight />} />
          <Route path="/blame" element={<BlameLinksScene />} />
            <Route path="/blame/list" element={<BlameListScene />} />
            <Route path="/blame/blame/:id1/:id2" element={ <BlameBlameScene />}/>
       
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
